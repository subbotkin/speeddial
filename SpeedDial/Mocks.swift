import Foundation

struct Mocks {
    static let tilesJson = """
    {
    "tiles": [
        {
            "img": "https://api.alohabrowser.com/static/speed_dial_tile/2017-08/2017-08-24%2010:29:08.png",
            "url": "http://help.alohabrowser.com/download.html",
            "modal": true,
            "type": "info",
            "id": 7
        },
        {
            "img": "https://api.alohabrowser.com/static/speed_dial_tile/2017-08/2017-08-30%2015:25:50.png",
            "url": "http://help.alohabrowser.com/features.html",
            "modal": true,
            "type": "info",
            "id": 8
        },
        {
            "img": "https://api.alohabrowser.com/static/speed_dial_tile/2017-08/2017-08-30%2015:25:50.png",
            "url": "http://help.alohabrowser.com/features.html",
            "modal": true,
            "type": "info",
            "id": 8
        },
        {
            "img": "https://api.alohabrowser.com/static/speed_dial_tile/2017-08/2017-08-30%2015:25:50.png",
            "url": "http://help.alohabrowser.com/features.html",
            "modal": true,
            "type": "info",
            "id": 8
        }
    ]
    }
    """
}

extension Mocks {
    static var tiles: [Tile] {
        guard let newsJSONData = tilesJson.data(using: .utf8) else {
            fatalError("Wrong JSON")
        }
        let jsonDecoder = JSONDecoder()
        do {
            let mocks = try jsonDecoder.decode([String: [Tile]].self, from: newsJSONData)
            return mocks["tiles", default: []]
        } catch {
            fatalError(error.localizedDescription)
        }

    }
}

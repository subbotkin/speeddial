//
//  AppDelegate.swift
//  SpeedDial
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        print("\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true))")
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = SpeedDialViewController(viewModel: SpeedDialViewModel())
//        window?.rootViewController = AddressBarTestViewController()
        window?.makeKeyAndVisible()
        return true
    }
}


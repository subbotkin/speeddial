//
//  AddressBarTestViewController.swift
//  SpeedDial
//
//  Created by Alexey Subbotkin on 27/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import Action
import RxSwift
import RxCocoa

final class AddressBarTestViewController: UIViewController {
    
    lazy var addressBar: AddressBar = {
       let addressBar = AddressBar()
        
        return addressBar
    }()
    
    lazy var showButton: UIButton = {
        let showButton = UIButton(type: .system)
        showButton.setTitle("show", for: .normal)
        
        return showButton
    }()
    
    lazy var hideButton: UIButton = {
        let hideButton = UIButton(type: .system)
        hideButton.setTitle("hide", for: .normal)
        
        return hideButton
    }()
    
    let addressBarViewModel = AddressBarViewModel()
    
    override func viewDidLoad() {
        view.addSubview(addressBar)
        view.backgroundColor = .gray
        
        addressBar.backgroundColor = .white
        addressBar.snp.makeConstraints { make in
            make.top.equalTo(view).offset(100)
            make.leading.equalTo(view)
            make.trailing.equalTo(view)
            make.height.equalTo(50)
        }
        let layer = addressBar.layer
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowRadius = 23
        
        
        addressBar.bind(to: addressBarViewModel)
        
        view.addSubview(showButton)
        view.addSubview(hideButton)
        
        showButton.snp.makeConstraints { make in
            make.center.equalTo(view)
        }
        
        hideButton.snp.makeConstraints { make in
            make.centerX.equalTo(view)
            make.top.equalTo(showButton.snp.bottom)
        }
        
        showButton.rx.bind(to: addressBarViewModel.shown, input: false)
        hideButton.rx.bind(to: addressBarViewModel.shown, input: true)
    }
    
}

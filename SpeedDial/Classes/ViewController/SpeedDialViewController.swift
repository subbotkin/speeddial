//
//  SpeedDialViewController.swift
//  AlohaBrowser
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 Aloha Mobile Ltd. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SnapKit

extension UIScrollView {
    func  isNearBottomEdge(edgeOffset: CGFloat = 20.0) -> Bool {
        return self.contentOffset.y + self.frame.size.height + edgeOffset > self.contentSize.height
    }
}

final class SpeedDialViewController: UIViewController {

    // MARK: - UIProperties

    private var customLayout: SpeedDialCollectionViewLayout? {
        return collectionView.collectionViewLayout as? SpeedDialCollectionViewLayout
    }

    private let collectionView: UICollectionView = {
        let layout = SpeedDialCollectionViewLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = nil
        collectionView.alwaysBounceVertical = true
        collectionView.showsVerticalScrollIndicator = false
        return collectionView
    }()

    // MARK: - Properties

    private let disposeBag = DisposeBag()
    let viewModel: SpeedDialViewModel

    init(viewModel: SpeedDialViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        weak var weakSelf = self
        self.viewModel.set(adViewController: weakSelf)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Should use init(viewModel: ")
    }

    // MARK: - VC Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        configureCollectionView()
        bindViewModel()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.isActive = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.isActive = false
    }
    
    // MARK: - Public

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }

    // MARK: - Private

    private func configureCollectionView() {
        view.addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            // TODO: Check landscape for iPhone X
            make.edges.equalTo(view.snp.edges)
        }

        collectionView.register(viewType: SpeedDialHeaderView.self,
                                withKind: SpeedDialCollectionViewLayout.CustomElementKind.headerKind)
        collectionView.register(viewType: AddressBarSupplementaryView.self,
                                withKind: SpeedDialCollectionViewLayout.CustomElementKind.addressBarKind)
        collectionView.register(viewType: SpeedDialSectionHeader.self,
                                withKind: UICollectionElementKindSectionHeader)
        collectionView.register(cellType: SpeedDialItemCollectionViewCell.self)
        collectionView.register(cellType: FeedItemCollectionViewCell.self)
        collectionView.register(cellType: GoogleContentAdCell.self)
        collectionView.register(cellType: GoogleAppInstallAdCell.self)
        collectionView.register(cellType: FacebookNativeAdCell.self)
        collectionView.register(cellType: NewsLoadingCollectionViewCell.self)

        customLayout?.settings = SpeedDialLayoutSettings()
        customLayout?.settings.headerSize = CGSize(width: view.frame.width, height: view.frame.width * 0.7)
        customLayout?.settings.sectionsHeaderSize = CGSize(width: view.frame.width, height: 20)
        customLayout?.settings.bookmarkItemSize = CGSize(width: 55, height: 79)
        customLayout?.settings.newsItemSize = CGSize(width: view.frame.width, height: 72)
        customLayout?.settings.sectionInsets = UIEdgeInsets(top: 40, left: 16, bottom: 0, right: 16)
        customLayout?.settings.minimumInterItemSpacing = 16
        customLayout?.settings.minimumLineSpacing = 24
        customLayout?.settings.sectionsLineSpacing = 50
        customLayout?.settings.newsLineSpacing = 16
        customLayout?.feedHeightCalculator = FeedHeightCalculator(feedProvider: viewModel)
        
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        }
    }

    private func bindViewModel() {
        weak var weakViewController = self
        let dataSource = RxCollectionViewSectionedAnimatedDataSource<SpeedDialSectionModel>(
            configureCell: SpeedDialViewController.cellConfiguration(with: weakViewController),
            configureSupplementaryView: SpeedDialViewController
                .supplementaryViewConfiguration(with: viewModel.speedDialHeaderViewModel))
        
        viewModel
            .speedDialSection
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)

        collectionView.rx.contentOffset.asDriver()
            .flatMap { _ -> Driver<()> in self.collectionView.isNearBottomEdge(edgeOffset: 20) ? .just(()) : .empty() }
            .drive(viewModel.loadMoreTrigger)
            .disposed(by: disposeBag)

        viewModel.speedDialHeaderViewModel.onTileTap = { [weak self] tileViewModel in
            let tileViewController = ModalTileViewController(tileViewModel: tileViewModel)
            self?.present(tileViewController, animated: true, completion: nil)
        }

        viewModel.speedDialHeaderViewModel.openSettings = {
            print("Open settings")
        }
    }
}

fileprivate extension SpeedDialViewController {

    static func cellConfiguration(with viewController: UIViewController?) ->
        CollectionViewSectionedDataSource<SpeedDialSectionModel>.ConfigureCell {
            return { (dataSource, collectionView, indexPath, item) -> UICollectionViewCell in
                switch item {
                case .speedDialBookmarkItem(let bookmarkViewModel):
                    let itemCell = collectionView.dequeueCell(SpeedDialItemCollectionViewCell.self, for: indexPath)
                    itemCell.bind(to: bookmarkViewModel)
                    return itemCell
                case .frequentlyVisitedItem(let frequentlyVisitedViewModel):
                    let itemCell = collectionView.dequeueCell(SpeedDialItemCollectionViewCell.self, for: indexPath)
                    itemCell.bind(to: frequentlyVisitedViewModel)
                    return itemCell
                case .feedItem(let feedItem):
                    switch feedItem {
                    case .loading:
                        return collectionView.dequeueCell(NewsLoadingCollectionViewCell.self, for: indexPath)
                    case .news(let newsViewModel):
                        let newsCell = collectionView.dequeueCell(FeedItemCollectionViewCell.self, for: indexPath)
                        newsCell.bind(to: newsViewModel)
                        return newsCell
                    case .advertisment(let adViewModel):
                        let adCell: UICollectionViewCell & AdCell
                        switch adViewModel.adModel.type {
                        case .googleContent:
                            adCell = collectionView.dequeueCell(GoogleContentAdCell.self, for: indexPath)
                        case .googleAppInstall:
                            adCell = collectionView.dequeueCell(GoogleAppInstallAdCell.self, for: indexPath)
                        case .facebook:
                            adCell = collectionView.dequeueCell(FacebookNativeAdCell.self, for: indexPath)
                        }
                        adCell.bind(to: adViewModel, with: viewController)
                        return adCell
                    }
                }
            }
    }

    static func supplementaryViewConfiguration(with headerViewModel: SpeedDialHeaderViewModel) ->
        CollectionViewSectionedDataSource<SpeedDialSectionModel>.ConfigureSupplementaryView {
            return { (dataSource, collectionView, kind, indexPath) -> UICollectionReusableView in
                switch kind {
                case SpeedDialCollectionViewLayout.CustomElementKind.headerKind:
                    let header = collectionView.dequeueReusableSupplementaryView(SpeedDialHeaderView.self,
                                                                                 ofKind: kind,
                                                                                 for: indexPath)
                    header.bind(to: headerViewModel)
                    return header
                case SpeedDialCollectionViewLayout.CustomElementKind.addressBarKind:
                    let header = collectionView.dequeueReusableSupplementaryView(AddressBarSupplementaryView.self,
                                                                                 ofKind: kind,
                                                                                 for: indexPath)
                    header.bind(to: headerViewModel)
                    return header
                case UICollectionElementKindSectionHeader:
                    let sectionHeaderView = collectionView.dequeueReusableSupplementaryView(SpeedDialSectionHeader.self,
                                                                                            ofKind: kind,
                                                                                            for: indexPath)

                    let section = dataSource.sectionModels[indexPath.section]
                    sectionHeaderView.titleLabel.text = section.title
                    return sectionHeaderView
                default:
                    fatalError("Unexpected element kind")
                }
            }
    }
}

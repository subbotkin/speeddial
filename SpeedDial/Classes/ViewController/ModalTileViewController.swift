//
//  ModalTileViewController.swift
//  SpeedDial
//
//  Created by ib on 23/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import WebKit

final class ModalTileViewController: UIViewController {

    // MARK: - Properties

    private let disposeBag = DisposeBag()
    private(set) var tileViewModel: ModalTileViewModel!

    // MARK: - UIProperties

    private let tileImageView = UIImageView()
    private let webContentView = UIWebView(frame: .zero)

    // MARK: - Init

    convenience init(tileViewModel: ModalTileViewModel) {
        self.init(nibName: nil, bundle: nil)
        self.tileViewModel = tileViewModel
    }

    // MARK: - VC Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        precondition(tileViewModel != nil, "Tile must be setted up")
        self.view.backgroundColor = .white
        configureViews()
        placeViews()
        bindViewModal()
    }

    // MARK: - Private

    private func bindViewModal() {
        self.tileViewModel.tileImage.drive(tileImageView.rx.image).disposed(by: disposeBag)
        if let tileUrl = tileViewModel.tileLoadUrl {
            let request = URLRequest(url: tileUrl)
            self.webContentView.loadRequest(request)
        } else {
            assertionFailure("Tile has no URL")
        }
    }

    private func configureViews() {
        tileImageView.translatesAutoresizingMaskIntoConstraints = false
        webContentView.translatesAutoresizingMaskIntoConstraints = false
        webContentView.backgroundColor = nil
        webContentView.delegate = self
        webContentView.scrollView.showsVerticalScrollIndicator = false
        webContentView.scrollView.showsHorizontalScrollIndicator = false
        webContentView.scrollView.contentInset = UIEdgeInsets(top: view.bounds.width / 2.5,
                                                              left: 0,
                                                              bottom: 0,
                                                              right: 0)
    }

    private func placeViews() {
        view.addSubview(webContentView)
        webContentView.scrollView.addSubview(tileImageView)

        tileImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(-view.bounds.width / 2.5)
            make.leading.equalTo(view)
            make.trailing.equalTo(view)
            make.height.equalTo(tileImageView.snp.width).dividedBy(2.5)
        }

        webContentView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
}

extension ModalTileViewController: UIWebViewDelegate {
    func webView(_ webView: UIWebView,
                 shouldStartLoadWith request: URLRequest,
                 navigationType: UIWebViewNavigationType) -> Bool {
        if request.url == tileViewModel.tileLoadUrl {
            return true
        } else {
            // TODO: Load in main browser
            return false
        }
    }
}

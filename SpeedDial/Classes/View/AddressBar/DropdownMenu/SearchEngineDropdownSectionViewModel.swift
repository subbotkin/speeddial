//
//  SearchEngineDropdownSectionViewModel.swift
//  AddressBarDropdownMenu
//
//  Created by Alexey Subbotkin on 29/11/2017.
//  Copyright © 2017 Alexey Subbotkin. All rights reserved.
//

import Foundation
import RxSwift
import RxDataSources

struct SearchEngineDropdownSectionViewModel: SectionModelType {
    typealias Item = SearchEngineDropdownCellViewModel
    
    var items: [SearchEngineDropdownCellViewModel]
}

extension SearchEngineDropdownSectionViewModel {
    init(original: SearchEngineDropdownSectionViewModel, items: [SearchEngineDropdownCellViewModel]) {
        self = original
        self.items = items
    }
}

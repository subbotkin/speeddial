//
//  SearchEngineDropdown.swift
//  AddressBarDropdownMenu
//
//  Created by Alexey Subbotkin on 29/11/2017.
//  Copyright © 2017 Alexey Subbotkin. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import SnapKit
import RxDataSources

final class SearchEngineDropdown: UIView {
    
    private let disposeBag = DisposeBag()
    
    // MARK: - Public Properties
    
    var viewModel: SearchEngineDropdownViewModel? {
        didSet {
            if let viewModel = viewModel {
                bind(to: viewModel)
            }
        }
    }
    
    // MARK: - Private Properties
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero,
                                              collectionViewLayout: collectionViewLayout)
        collectionView.register(SearchEngineDropdownCell.self,
                                forCellWithReuseIdentifier: "SearchEngineDropdownCell")
        collectionView.backgroundColor = .white
        
        return collectionView
    }()
    
    private lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let collectionViewLayout = UICollectionViewFlowLayout()
        collectionViewLayout.itemSize = CGSize(width: 100, height: 100)
        
        return collectionViewLayout
    }()
    
    private lazy var dataSource: RxCollectionViewSectionedReloadDataSource<SearchEngineDropdownSectionViewModel> = {
        return RxCollectionViewSectionedReloadDataSource(configureCell: { (_, collectionView, indexPath, item) -> UICollectionViewCell in
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchEngineDropdownCell", for: indexPath) as? SearchEngineDropdownCell else {
                fatalError()
            }
            
            cell.bind(to: item)
            
            return cell
        }, configureSupplementaryView: { (_, _, _, _) -> UICollectionReusableView in
            return UICollectionReusableView()
        })
    }()
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupViews()
    }
    
    private func setupViews() {
        addSubview(collectionView)
        
        collectionView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
        
        collectionView.rx.itemSelected.subscribe(onNext: { item in
            print(item)
        }).disposed(by: disposeBag)
    }
    
    // MARK: - UIView
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let itemSize = CGSize(width: bounds.size.width / 5, height: bounds.size.height / 2)
        
        collectionViewLayout.itemSize = itemSize
        collectionViewLayout.minimumInteritemSpacing = 0
        collectionViewLayout.minimumLineSpacing = 0
    }
    
    // MARK: - Public Methods
    
    // MARK: - Private Methods
    
    private func bind(to viewModel: SearchEngineDropdownViewModel) {
        viewModel
            .sections
            .bind(to: collectionView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
    }
}

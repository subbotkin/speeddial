//
//  SearchEngineDropdownViewModel.swift
//  AddressBarDropdownMenu
//
//  Created by Alexey Subbotkin on 29/11/2017.
//  Copyright © 2017 Alexey Subbotkin. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

final class SearchEngineDropdownViewModel {
    var sections = BehaviorRelay<[SearchEngineDropdownSectionViewModel]>(value: SearchEngineDropdownViewModel.mockSections)
    
    private static var mockSections: [SearchEngineDropdownSectionViewModel] {
        let items = [#imageLiteral(resourceName: "google"), #imageLiteral(resourceName: "yahoo"), #imageLiteral(resourceName: "bing"), #imageLiteral(resourceName: "duckduck"), #imageLiteral(resourceName: "yahooJp"),
                     #imageLiteral(resourceName: "baidu"), #imageLiteral(resourceName: "yandex"), #imageLiteral(resourceName: "wikipedia"), #imageLiteral(resourceName: "youtube"), #imageLiteral(resourceName: "amazone")]
        .map(SearchEngineDropdownCellViewModel.init)
        
        return [SearchEngineDropdownSectionViewModel(items: items)]
    }
}

//
//  SearchEngineDropdownCellViewModel.swift
//  AddressBarDropdownMenu
//
//  Created by Alexey Subbotkin on 29/11/2017.
//  Copyright © 2017 Alexey Subbotkin. All rights reserved.
//

import Foundation
import RxDataSources

struct SearchEngineDropdownCellViewModel {
    var image: UIImage
}

//
//  SearchEngineDropdownCell.swift
//  AddressBarDropdownMenu
//
//  Created by Alexey Subbotkin on 29/11/2017.
//  Copyright © 2017 Alexey Subbotkin. All rights reserved.
//

import UIKit
import SnapKit

final class SearchEngineDropdownCell: UICollectionViewCell {
    
    lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .center
        
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func setup() {
        contentView.addSubview(imageView)
        
        imageView.snp.makeConstraints { make in
            make.edges.equalTo(contentView)
        }
    }
    
    func bind(to viewModel: SearchEngineDropdownCellViewModel) {
        imageView.image = viewModel.image
    }
    
}

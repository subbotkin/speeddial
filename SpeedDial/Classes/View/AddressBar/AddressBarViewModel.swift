//
//  AddressBarViewModel.swift
//  SpeedDial
//
//  Created by Alexey Subbotkin on 27/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Action

final class AddressBarViewModel {
    var qrReaderButtonHidden = BehaviorRelay<Bool>(value: false)
    var cancelButtonHidden = BehaviorRelay<Bool>(value: true)
    var secureConnectionButtonHidden = BehaviorRelay<Bool>(value: false)
    var searchEngineButtonHidden = BehaviorRelay<Bool>(value: false)
    
    var textFieldFocused = BehaviorRelay<Bool>(value: false)
    
    lazy var shown: Action<Bool, Void> = Action(workFactory: { [weak self] shown in
        self?.secureConnectionButtonHidden.accept(shown)
        self?.searchEngineButtonHidden.accept(shown)
        self?.cancelButtonHidden.accept(!shown)
        self?.qrReaderButtonHidden.accept(shown)
        self?.textFieldFocused.accept(shown)
        return .empty()
    })
}

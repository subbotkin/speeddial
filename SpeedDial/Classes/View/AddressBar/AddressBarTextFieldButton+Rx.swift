//
//  AddressBarTextFieldButton+Rx.swift
//  SpeedDial
//
//  Created by Alexey Subbotkin on 29/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

extension Reactive where Base: AddressBarTextFieldButton {
    var state: Binder<AddressBarTextFieldButton.State> {
        return Binder(self.base) { button, state in
            button.state = state
        }
    }
}

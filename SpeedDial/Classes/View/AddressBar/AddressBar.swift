//
//  AddressBar.swift
//  SpeedDial
//
//  Created by Alexey Subbotkin on 27/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxAnimated
import Action

final class AddressBar: UIView {
    
    lazy var secureConnectionButton: UIButton = {
        let secureConnectionButton = UIButton(type: .custom)
        secureConnectionButton.imageView?.contentMode = .center
        secureConnectionButton.imageView?.clipsToBounds = false
        secureConnectionButton.setImage(#imageLiteral(resourceName: "vpnOn"), for: .normal)
        
        return secureConnectionButton
    }()
    
    lazy var separator: UIView = {
        let separator = UIView()
        separator.backgroundColor = UIColor(red: 178.0 / 255.0, green: 192.0 / 255.0, blue: 210.0 / 255.0, alpha: 0.44)
        
        return separator
    }()
    
    lazy var searchEngineButton: UIButton = {
        let searchEngineButton = UIButton(type: .custom)
        searchEngineButton.imageView?.contentMode = .center
        searchEngineButton.imageView?.clipsToBounds = false
        searchEngineButton.setImage(#imageLiteral(resourceName: "yahooJp"), for: .normal)
        
        return searchEngineButton
    }()
    
    lazy var qrReaderButton: AddressBarTextFieldButton = {
        let qrReaderButton = AddressBarTextFieldButton()
        
        return qrReaderButton
    }()
    
    lazy var cancelButton: UIButton = {
        let cancelButton = UIButton(type: .custom)
        cancelButton.titleLabel?.contentMode = .center
        cancelButton.titleLabel?.clipsToBounds = false
        cancelButton.clipsToBounds = false
        cancelButton.setTitle("Cancel", for: .normal)
        cancelButton.setTitleColor(.black, for: .normal)
        
        return cancelButton
    }()
    
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.text = "Type something"
        titleLabel.textColor = UIColor(red: 0.80, green: 0.80, blue: 0.80, alpha: 1.0)
        
        return titleLabel
    }()
    
    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Type anything"
        
        return textField
    }()
    
    let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup() {
        let stackView = UIStackView()
//        stackView.distribution = .fillProportionally
        stackView.alignment = .center
        
        addSubview(stackView)
        stackView.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }

        stackView.addArrangedSubview(secureConnectionButton)
        stackView.addArrangedSubview(separator)
        stackView.addArrangedSubview(searchEngineButton)
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(qrReaderButton)
        stackView.addArrangedSubview(cancelButton)
        
        secureConnectionButton.snp.makeConstraints { make in
            make.width.equalTo(50).priority(750)
        }
        
        separator.snp.makeConstraints { make in
            make.width.equalTo(1).priority(750)
            make.top.equalTo(stackView).offset(13)
            make.bottom.equalTo(stackView).offset(-13)
        }
        
        searchEngineButton.snp.makeConstraints { make in
            make.width.equalTo(50).priority(750)
        }
        
        qrReaderButton.snp.makeConstraints { make in
            make.width.equalTo(50).priority(750)
        }
        
        cancelButton.snp.makeConstraints { make in
            make.width.equalTo(30).priority(750)
        }

        layoutIfNeeded()
    }
    
    func bind(to viewModel: AddressBarViewModel) {
        viewModel
            .searchEngineButtonHidden
            .distinctUntilChanged()
            .asObservable()
            .bind(animated: searchEngineButton.rx.animated.isHiddenWithAlpha)
            .disposed(by: disposeBag)
        
        Observable.combineLatest(viewModel.searchEngineButtonHidden, viewModel.secureConnectionButtonHidden)
            .map { searchEngineButtonHidden, secureConnectionButtonHidden -> Bool in
                searchEngineButtonHidden && secureConnectionButtonHidden
                
            }.distinctUntilChanged()
            .asObservable()
            .bind(animated: separator.rx.animated.isHiddenWithAlpha)
            .disposed(by: disposeBag)
        
        viewModel
            .secureConnectionButtonHidden
            .distinctUntilChanged()
            .asObservable()
            .bind(animated: secureConnectionButton.rx.animated.isHiddenWithAlpha)
            .disposed(by: disposeBag)
        
        viewModel
            .cancelButtonHidden
            .distinctUntilChanged()
            .asObservable()
            .bind(animated: cancelButton.rx.animated.isHiddenWithAlpha)
            .disposed(by: disposeBag)
        
        viewModel
            .qrReaderButtonHidden
            .distinctUntilChanged()
            .asObservable()
            .bind(animated: qrReaderButton.rx.animated.isHiddenWithAlpha)
            .disposed(by: disposeBag)
        
        textField.rx.controlEvent(UIControlEvents.editingDidBegin).asDriver().drive(onNext: { _ in
            viewModel.shown.execute(true)
        }).disposed(by: disposeBag)
        
        cancelButton.rx.bind(to: viewModel.shown, input: false)
        
        viewModel.textFieldFocused.asDriver().drive(onNext: { [weak self] focused in
            if !focused {
                self?.textField.resignFirstResponder()
            } else {
                if self?.textField.isFirstResponder == false {
                    self?.textField.becomeFirstResponder()
                }
            }
        }).disposed(by: disposeBag)
    }
}

extension AnimatedSink where Base: UIView {
    func emptyAnimation(duration: TimeInterval) -> AnimatedSink<Base> {
        let animation = AnimationType<Base>(type: .animation, duration: duration, animations: { _ in })
        return AnimatedSink<Base>(base: self.base, type: animation)
    }
}

extension AnimatedSink where Base: UIView {
    public var isHiddenWithAlpha: Binder<Bool> {
        return Binder(self.base) { view, hidden in
            UIView.animate(withDuration: 0.3, animations: {
                view.alpha = hidden ? 0 : 1
                view.isHidden = hidden
            })
        }
    }
}

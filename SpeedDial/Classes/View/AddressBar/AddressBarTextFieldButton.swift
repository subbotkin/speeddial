//
//  AddressBarTextFieldButton.swift
//  DemoAddressBarTextFieldButton
//
//  Created by Alexey Subbotkin on 28/11/2017.
//  Copyright © 2017 Alexey Subbotkin. All rights reserved.
//

import Foundation
import UIKit
import SnapKit
import pop

final class AddressBarTextFieldButton: UIView {
    
    enum State {
        case none
        case reload
        case stop
        case clear
        case qr
    }
    
    // MARK: - Public Properties
    
    var state: State = .reload {
        didSet {
            if state != oldValue {
                animate()
            }
        }
    }
    
    var animationDuration: TimeInterval = 0.3
    
    // MARK: - Private Properties
    
    private var image: UIImage? {
        switch state {
        case .none: return nil
        case .reload: return #imageLiteral(resourceName: "iconReloadPage")
        case .stop: return #imageLiteral(resourceName: "iconWebClose")
        case .clear: return #imageLiteral(resourceName: "iconDel")
        case .qr: return #imageLiteral(resourceName: "iconQrCodeOnTap")
        }
    }

    private lazy var button: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(image, for: .normal)
        button.imageView?.clipsToBounds = false
        button.imageView?.contentMode = .center
        
        return button
    }()
    
    // MARK: - Initializers
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupViews()
    }
    
    private func setupViews() {
        addSubview(button)
        
        button.snp.makeConstraints { make in
            make.edges.equalTo(self)
        }
        
        button.layoutIfNeeded()
    }
    
    var scaleAnimation: POPBasicAnimation? {
        return button.layer.pop_animation(forKey: "scale") as? POPBasicAnimation
    }
    
    // MARK: - Public Methods
    
    func animate() {
        button.layer.pop_removeAllAnimations()
        guard let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPLayerScaleXY) else { return }
        scaleAnimation.duration = animationDuration / 2
        scaleAnimation.toValue = CGSize(width: 0.3, height: 0.3)
        scaleAnimation.completionBlock = { _, _ in
            guard let scaleAnimation = POPBasicAnimation(propertyNamed: kPOPLayerScaleXY) else { return }
            self.button.setImage(self.image, for: .normal)
            scaleAnimation.duration = self.animationDuration / 2
            scaleAnimation.toValue = CGSize(width: 1, height: 1)
            self.button.layer.pop_add(scaleAnimation, forKey: "scale")
        }
        
        button.layer.pop_add(scaleAnimation, forKey: "scale")
    }
    
}

//
//  TileCollectionVIewFlowLayout.swift
//  SpeedDial
//
//  Created by ib on 21/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class TileCollectionViewFlowLayout: UICollectionViewFlowLayout {

    private var inset: CGFloat = 0.0
    private var interItemSpace: CGFloat = 0.0

    convenience init(inset: CGFloat = 0.0, interItemSpace: CGFloat = 0.0) {
        self.init()
        self.inset = inset
        self.interItemSpace = interItemSpace
    }

    override open func prepare() {

        guard let collectionViewSize = collectionView?.frame.size else { return }

        // Set itemSize based on total width and inset
        itemSize = collectionViewSize
        itemSize.width -= 2 * (inset + interItemSpace)

        // Set scrollDirection and paging
        scrollDirection = .horizontal
        collectionView?.isPagingEnabled = true

        minimumLineSpacing = interItemSpace
        minimumInteritemSpacing = interItemSpace

        sectionInset = UIEdgeInsets(top: 0.0, left: inset + interItemSpace, bottom: 0.0, right: inset + interItemSpace)
        footerReferenceSize = CGSize.zero
        headerReferenceSize = CGSize.zero
    }
}

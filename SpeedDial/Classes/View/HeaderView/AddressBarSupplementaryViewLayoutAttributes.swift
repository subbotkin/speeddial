//
//  AddressBarSupplementaryViewLayoutAttributes.swift
//  SpeedDial
//
//  Created by Alexey Subbotkin on 28/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class AddressBarSupplementaryViewLayoutAttributes: SpeedDialLayoutAttributes {
    var progress: CGFloat = 0
}

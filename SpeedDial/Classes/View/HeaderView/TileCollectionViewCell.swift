//
//  HeaderTileView.swift
//  SpeedDial
//
//  Created by ib on 20/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class TileCollectionViewCell: UICollectionViewCell, BaseCollectionViewCell {

    // MARK: - UIProperties

    private let tileImageView = UIImageView()
    private var closeButton = UIButton()
    private let infoImageView = UIImageView()

    // MARK: - Properties

    private var reuseDisposeBag = DisposeBag()
    var tileViewModel: TileViewModel?

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
        placeViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        reuseDisposeBag = DisposeBag()
        tileViewModel = nil
        tileImageView.image = nil
        infoImageView.image = nil
        super.prepareForReuse()
    }

    // MARK: - Public

    func bind(to viewModel: TileViewModel) {
        tileViewModel = viewModel
        closeButton.rx.bind(to: viewModel.onCloseButtonAction, input: viewModel.tile)

        viewModel.infoIcon
            .drive(infoImageView.rx.image)
            .disposed(by: reuseDisposeBag)

        viewModel.tileImage
            .drive(tileImageView.rx.image)
            .disposed(by: reuseDisposeBag)
    }

    // MARK: - Private

    private func configureViews() {
        backgroundColor = .yellow
        layer.cornerRadius = 5
        layer.masksToBounds = true
        tileImageView.contentMode = .scaleAspectFill
        closeButton.setImage(#imageLiteral(resourceName: "closeTile"), for: .normal)
        closeButton.imageView?.contentMode = .center
        infoImageView.image = #imageLiteral(resourceName: "infoTile")
    }

    private func placeViews() {
        tileImageView.translatesAutoresizingMaskIntoConstraints = false
        infoImageView.translatesAutoresizingMaskIntoConstraints = false
        closeButton.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(tileImageView)
        contentView.addSubview(closeButton)
        contentView.addSubview(infoImageView)

        tileImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        closeButton.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.trailing.equalToSuperview()
            make.size.equalTo(25)
        }

        infoImageView.snp.makeConstraints { (make) in
            make.centerX.equalTo(closeButton)
            make.bottom.equalToSuperview().inset(4)
        }
    }
}

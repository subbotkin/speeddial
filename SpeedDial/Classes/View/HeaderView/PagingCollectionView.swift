//
//  PagingCollectionView.swift
//  SpeedDial
//
//  Created by ib on 22/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class PagingCollectionView: UICollectionView {

    // MARK: - Properties

    var inset: CGFloat = 0.0 {
        didSet {
            configureLayout()
        }
    }

    var interItemSpacing: CGFloat = 0.0 {
        didSet {
            configureLayout()
        }
    }

    var currentCenterCell: UICollectionViewCell? {
        let lowerBound = inset - 20
        let upperBound = inset + 20

        for cell in visibleCells {
            let cellRect = convert(cell.frame, to: nil)
            if cellRect.origin.x > lowerBound && cellRect.origin.x < upperBound {
                return cell
            }
        }
        return nil
    }

    open var currentCenterCellIndex: IndexPath? {
        guard let currentCenterCell = self.currentCenterCell else { return nil }
        return indexPath(for: currentCenterCell)
    }

    override var contentSize: CGSize {
        didSet {
            guard
                let dataSource = dataSource,
                let invisibleScrollView = invisibleScrollView else {
                    return
            }
            let numberSections = dataSource.numberOfSections?(in: self) ?? 1
            var numberItems = 0
            for i in 0..<numberSections {
                let numberSectionItems = dataSource.collectionView(self, numberOfItemsInSection: i)
                numberItems += numberSectionItems
            }

            let contentWidth = invisibleScrollView.frame.width * CGFloat(numberItems)
            invisibleScrollView.contentSize = CGSize(width: contentWidth, height: invisibleScrollView.frame.height)
        }
    }

    // MARK: - Properties (Private)
    fileprivate var invisibleScrollView: UIScrollView!
    fileprivate var invisibleWidthConstraint: NSLayoutConstraint?
    fileprivate var invisibleLeftConstraint: NSLayoutConstraint?

    // MARK: - Lifecycle

    override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    convenience init(withFrame frame: CGRect, andInset inset: CGFloat) {
        self.init(frame: frame, collectionViewLayout: TileCollectionViewFlowLayout(inset: inset, interItemSpace: 0))
        self.inset = inset
    }

    // MARK: - Overrides

    override func scrollRectToVisible(_ rect: CGRect, animated: Bool) {
        invisibleScrollView.setContentOffset(rect.origin, animated: animated)
    }

    override func scrollToItem(at indexPath: IndexPath,
                               at scrollPosition: UICollectionViewScrollPosition,
                               animated: Bool) {
        super.scrollToItem(at: indexPath, at: scrollPosition, animated: animated)

        let originX = (CGFloat(indexPath.item) * (frame.size.width - 2 * (inset) - interItemSpacing))
        let rect = CGRect(x: originX, y: 0, width: frame.size.width - 2 * (inset) - interItemSpacing,
                          height: frame.height)
        scrollRectToVisible(rect, animated: animated)
    }

    override open func didMoveToSuperview() {
        super.didMoveToSuperview()

        addInvisibleScrollView(to: superview)
    }

    // MARK: - Public API

    func didScroll() {
        scrollViewDidScroll(self)
    }
}

private typealias PrivateAPI = PagingCollectionView
fileprivate extension PrivateAPI {

    fileprivate func addInvisibleScrollView(to superview: UIView?) {
        guard let superview = superview else { return }

        /// Add our 'invisible' scrollview
        invisibleScrollView = UIScrollView(frame: bounds)
        invisibleScrollView.translatesAutoresizingMaskIntoConstraints = false
        invisibleScrollView.isPagingEnabled = true
        invisibleScrollView.showsHorizontalScrollIndicator = false

        invisibleScrollView.isUserInteractionEnabled = false

        invisibleScrollView.delegate = self

        addGestureRecognizer(invisibleScrollView.panGestureRecognizer)

        superview.addSubview(invisibleScrollView)

        configureLayout()
    }

    fileprivate func configureLayout() {

        collectionViewLayout = TileCollectionViewFlowLayout(inset: inset, interItemSpace: interItemSpacing)

        guard let invisibleScrollView = invisibleScrollView else { return }

        invisibleScrollView.snp.remakeConstraints { (make) in
            make.height.equalTo(self)
            make.top.equalTo(self)
            make.width.equalTo(self).offset(-(2 * inset + interItemSpacing))
            make.leading.equalTo(self).offset(inset + interItemSpacing / 2)
        }
    }
}

private typealias InvisibleScrollDelegate = PagingCollectionView
extension InvisibleScrollDelegate: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateOffSet()
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        delegate?.scrollViewDidEndDecelerating?(scrollView)
    }

    func updateOffSet() {
        contentOffset = invisibleScrollView.contentOffset
    }
}

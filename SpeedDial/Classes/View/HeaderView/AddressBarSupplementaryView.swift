//
//  AddressBarSupplementaryView.swift
//  SpeedDial
//
//  Created by Alexey Subbotkin on 28/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Action
import SnapKit

final class AddressBarSupplementaryView: UICollectionReusableView, BaseCollectionReusableView {
    
    // MARK: - Properties
    
    private var reuseDisposeBag = DisposeBag()
    
    let addressBar = AddressBar()
    let dropDown = SearchEngineDropdown()
    
    var addressBarOffsetConstraint: Constraint!
    
    // MARK: - Init
    
    // MARK: - Public
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
        placeViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureViews() {
        addressBar.bind(to: AddressBarViewModel())
        dropDown.viewModel = SearchEngineDropdownViewModel()
    }
    
    func placeViews() {
        clipsToBounds = false
        
        addSubview(addressBar)
        addressBar.snp.makeConstraints { make in
            make.height.equalTo(50)
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            self.addressBarOffsetConstraint = make.centerY.equalTo(self).constraint
        }
        
        addSubview(dropDown)
        dropDown.snp.makeConstraints { make in
            make.height.equalTo(70)
            make.leading.equalTo(self)
            make.trailing.equalTo(self)
            make.top.equalTo(self.snp.bottom)
        }
    }
    
    func bind(to viewModel: SpeedDialHeaderViewModel) {
        backgroundColor = .white
        
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
//        layer.shadowOffset = CGSize(width: -1, height: 1)
        layer.shadowRadius = 23
        layer.cornerRadius = 4
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        
        guard let addressBarLayoutAttributes = layoutAttributes as? AddressBarSupplementaryViewLayoutAttributes else {
            return
        }
        
        addressBar.layer.zPosition = CGFloat(layoutAttributes.zIndex + 1)
        
        let progress = addressBarLayoutAttributes.progress
        addressBarOffsetConstraint.update(offset: 10 * progress)
        layer.cornerRadius = (1 - progress) * 4
    }
    
    // MARK: - Private
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let pointForTargetView: CGPoint = dropDown.convert(point, from: self)
        if dropDown.bounds.contains(pointForTargetView) {
            return dropDown
        }
        
        return super.hitTest(point, with: event)
    }
}

//
//  HeaderCollectionViewCell.swift
//  SpeedDial
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import Action

final class SpeedDialHeaderView: UICollectionReusableView, BaseCollectionReusableView {

    // MARK: - UIProperties

    private let themeImageView = UIImageView()
    private let roundCornerView = RoundCornersView()
    private let blurSetingsContainerView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
    private var settingsButton = UIButton(type: .system)
    private let tilesCollectionView = PagingCollectionView(withFrame: .zero, andInset: 0)

    // MARK: - Properties
    
    private var reuseDisposeBag = DisposeBag()
    var viewModel: SpeedDialHeaderViewModel?

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
        placeViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("User init(frame:)")
    }

    override func prepareForReuse() {
        reuseDisposeBag = DisposeBag()
        viewModel = nil
        themeImageView.image = nil
        settingsButton.rx.action = nil
        super.prepareForReuse()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let radius = min(roundCornerView.bounds.width, roundCornerView.bounds.height)
        roundCornerView.cornerRadius = CGSize(width: radius, height: radius)
    }

    // MARK: - Public

    func bind(to viewModel: SpeedDialHeaderViewModel) {
        self.viewModel = viewModel

        settingsButton.rx.action = viewModel.onSettingsTapAction

        viewModel.themeImage.asDriver(onErrorJustReturn: nil)
            .drive(themeImageView.rx.image)
            .disposed(by: reuseDisposeBag)

        let dataSource = RxCollectionViewSectionedAnimatedDataSource<TilesSection>(
            configureCell: SpeedDialHeaderView.cellConfiguration(),
            configureSupplementaryView: SpeedDialHeaderView.suplementaryViewConfiguration()
        )

        viewModel.tiles.asDriver(onErrorJustReturn: [])
            .drive(tilesCollectionView.rx.items(dataSource: dataSource))
            .disposed(by: reuseDisposeBag)

        let itemSelection = Observable.zip(tilesCollectionView.rx.itemSelected,
                                           tilesCollectionView.rx.modelSelected(TileViewModel.self))
            .share(replay: 1, scope: .whileConnected)

        itemSelection.asObservable()
            .ignoreWhen { [weak self] in self?.tilesCollectionView.currentCenterCellIndex == $0.0 }
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] (selectedItem) in
                self?.tilesCollectionView.scrollToItem(at: selectedItem.0,
                                                       at: .centeredHorizontally,
                                                       animated: true)
            })
            .disposed(by: reuseDisposeBag)

        itemSelection.asObservable()
            .ignoreWhen { [weak self] in self?.tilesCollectionView.currentCenterCellIndex != $0.0 }
            .map { $0.1.tile }
            .observeOn(MainScheduler.instance)
            .subscribe(viewModel.onTileTapAction.inputs)
            .disposed(by: reuseDisposeBag)
    }

    // MARK: - Private

    private func configureViews() {
        themeImageView.contentMode = .scaleAspectFill
        settingsButton.setImage(#imageLiteral(resourceName: "iconSettings"), for: .normal)

        tilesCollectionView.scrollsToTop = false
        tilesCollectionView.alwaysBounceHorizontal = true
        tilesCollectionView.backgroundColor = nil
        tilesCollectionView.showsHorizontalScrollIndicator = false
        tilesCollectionView.showsVerticalScrollIndicator = false
        tilesCollectionView.inset = 35
        tilesCollectionView.interItemSpacing = 8
        tilesCollectionView.register(cellType: TileCollectionViewCell.self)
        tilesCollectionView.rx.setDelegate(self).disposed(by: reuseDisposeBag)
    }

    private func placeViews() {
        themeImageView.translatesAutoresizingMaskIntoConstraints = false
        roundCornerView.translatesAutoresizingMaskIntoConstraints = false
        blurSetingsContainerView.translatesAutoresizingMaskIntoConstraints = false
        settingsButton.translatesAutoresizingMaskIntoConstraints = false
        tilesCollectionView.translatesAutoresizingMaskIntoConstraints = false

        addSubview(themeImageView)
        themeImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        blurSetingsContainerView.contentView.addSubview(settingsButton)
        roundCornerView.addSubview(blurSetingsContainerView)
        addSubview(roundCornerView)

        settingsButton.snp.makeConstraints { (make) in
            make.edges.equalToSuperview().inset(6)
            make.size.equalTo(15)
        }
        blurSetingsContainerView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        roundCornerView.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().inset(16)
            if #available(iOS 11.0, *) {
                make.top.equalTo(safeAreaLayoutGuide.snp.top).inset(8)
            } else {
                make.top.equalToSuperview().inset(28)
            }
        }

        addSubview(tilesCollectionView)
        tilesCollectionView.snp.makeConstraints { (make) in
            make.top.equalTo(roundCornerView.snp.bottom).offset(16)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.45)
        }
    }
}

extension SpeedDialHeaderView: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        tilesCollectionView.didScroll()
    }
}

private extension SpeedDialHeaderView {

    static func cellConfiguration() -> CollectionViewSectionedDataSource<TilesSection>.ConfigureCell {
            return { dataSource, collectionView, indexPath, item in
                let cell = collectionView.dequeueCell(TileCollectionViewCell.self, for: indexPath)
                cell.bind(to: item)
                return cell
            }
    }

    static func suplementaryViewConfiguration() ->
        CollectionViewSectionedDataSource<TilesSection>.ConfigureSupplementaryView {
        return { _, _, _, _ in return UICollectionReusableView() }
    }
}

//
//  SpeedDialSectionHeaderCollectionReusableView.swift
//  SpeedDial
//
//  Created by ib on 31/10/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class SpeedDialSectionHeader: UICollectionReusableView,  BaseCollectionReusableView {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            // TODO: Change to right color
            titleLabel.textColor = UIColor.blue
        }
    }
}

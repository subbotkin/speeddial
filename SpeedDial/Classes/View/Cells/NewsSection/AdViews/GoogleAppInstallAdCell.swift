//
//  GoogleAppInstallAdCell.swift
//  SpeedDial
//
//  Created by ib on 13/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMobileAds

final class GoogleAppInstallAdCell: UICollectionViewCell, BaseCollectionViewCell, AdCell {

    // MARK: - UIProperties

    @IBOutlet private weak var appInstallAdView: GoogleAppInstallAdView!

    // MARK: - Properties

    private var disposeBag = DisposeBag()
    var viewModel: AdViewModel?

    // MARK: - UICollectionViewCellLifeCycle

    override func prepareForReuse() {
        disposeBag = DisposeBag()
        if let adViewModel = viewModel {
            if case .googleAppInstall(let appInstallAd) = adViewModel.adModel.type {
                appInstallAd.unregisterAdView()
            } else {
                assertionFailure("Wrong ad type")
            }
        }
        appInstallAdView.titleLabel.text = nil
        appInstallAdView.subtitleLabel.text = nil
        appInstallAdView.adImageView.image = nil
        appInstallAdView.callToActionButton.setTitle(nil, for: .normal)
        viewModel = nil
        super.prepareForReuse()
    }

    // MARK: - Public

    func bind(to viewModel: AdViewModel, with viewController: UIViewController? = nil) {
        if case .googleAppInstall(let appInstallAd) = viewModel.adModel.type {
            self.viewModel = viewModel
            appInstallAdView.nativeAppInstallAd = appInstallAd
            appInstallAd.register(appInstallAdView,
                                  clickableAssetViews: [.callToActionAsset: appInstallAdView.callToActionButton],
                                  nonclickableAssetViews: [:])

            viewModel.title.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.titleTextAttributes) }
                .drive(appInstallAdView.titleLabel.rx.attributedText)
                .disposed(by: disposeBag)

            viewModel.subtitle.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.subtitleTextAttributes) }
                .drive(appInstallAdView.subtitleLabel.rx.attributedText)
                .disposed(by: disposeBag)

            viewModel.callToAction.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.callToActionTextAttributes) }
                .drive(appInstallAdView.callToActionButton.rx.attributedTitle(for: .normal))
                .disposed(by: disposeBag)


            viewModel.adImage.asDriver(onErrorJustReturn: nil)
                .drive(appInstallAdView.adImageView.rx.image)
                .disposed(by: disposeBag)
        } else {
            assertionFailure("Not supported type")
        }
    }
}

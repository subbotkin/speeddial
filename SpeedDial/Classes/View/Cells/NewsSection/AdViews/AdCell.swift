//
//  AdCell.swift
//  SpeedDial
//
//  Created by ib on 13/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import UIKit

//swiftlint:disable line_length
enum AdCellAttributes {
    // TODO: Divide in separate components (leading to image, size of image ...) and rewrite ad cells in code
    static let textContentHorizontalInsets: CGFloat = 104 + 28 // 104 - Image + offsets, 28 - right offset
    static let callToActionVerticalInsets: CGFloat = 4 + 8 + 10 // 4 - top space to subtitle, 8 - bottom space to content, 10 - titleInsets
}

protocol AdCell: class {
    func bind(to viewModel: AdViewModel, with viewController: UIViewController?)
}

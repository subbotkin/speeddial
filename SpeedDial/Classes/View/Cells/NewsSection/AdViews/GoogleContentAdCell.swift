//
//  GoogleContentAdCell.swift
//  SpeedDial
//
//  Created by ib on 13/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import GoogleMobileAds

final class GoogleContentAdCell: UICollectionViewCell, BaseCollectionViewCell, AdCell {

    // MARK: - UIProperties

    @IBOutlet private weak var contentAdView: GoogleContentAdView!

    // MARK: - Properties

    private var disposeBag = DisposeBag()
    var viewModel: AdViewModel?

    // MARK: - UICollectionViewCellLifeCycle

    override func prepareForReuse() {
        disposeBag = DisposeBag()
        if let adViewModel = viewModel {
            if case .googleContent(let contentAd) = adViewModel.adModel.type {
                contentAd.unregisterAdView()
            } else {
                assertionFailure("Wrong ad type")
            }
        }

        contentAdView.titleLabel.text = nil
        contentAdView.subtitleLabel.text = nil
        contentAdView.adImageView.image = nil
        contentAdView.callToActionButton.setTitle(nil, for: .normal)
        viewModel = nil
        super.prepareForReuse()
    }

    // MARK: - Public

    func bind(to viewModel: AdViewModel, with viewController: UIViewController? = nil) {
        if case .googleContent(let googleContentAd) = viewModel.adModel.type {
            self.viewModel = viewModel
            contentAdView.nativeContentAd = googleContentAd
            googleContentAd.register(contentAdView,
                                     clickableAssetViews: [.callToActionAsset: contentAdView.callToActionButton],
                                     nonclickableAssetViews: [:])

            viewModel.title.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.titleTextAttributes) }
                .drive(contentAdView.titleLabel.rx.attributedText)
                .disposed(by: disposeBag)

            viewModel.subtitle.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.subtitleTextAttributes) }
                .drive(contentAdView.subtitleLabel.rx.attributedText)
                .disposed(by: disposeBag)

            viewModel.callToAction.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.callToActionTextAttributes) }
                .drive(contentAdView.callToActionButton.rx.attributedTitle(for: .normal))
                .disposed(by: disposeBag)

            viewModel.adImage.asDriver(onErrorJustReturn: nil)
                .drive(contentAdView.adImageView.rx.image)
                .disposed(by: disposeBag)
        } else {
            assertionFailure("Not supported type")
        }
    }
    
}

//
//  FacebookNativeAdCell.swift
//  SpeedDial
//
//  Created by ib on 13/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import FBAudienceNetwork
import RxSwift
import RxCocoa
import RxSwiftExt

final class FacebookNativeAdCell: UICollectionViewCell, BaseCollectionViewCell, AdCell {

    // MARK: - UIProperties

    private let roundCornersContainer = UIView()
    private let adImageView = UIImageView()
    private let titleLabel = UILabel()
    private let subtitleLabel = UILabel()
    private let callToActionButton = UIButton(type: .system)
    private let sponsoredLabel = UILabel()
    private let adChoicesView = FBAdChoicesView(frame: .zero)

    // MARK: - Porperties

    private var disposeBag = DisposeBag()
    var viewModel: AdViewModel?

    // MARK: - UICollectionViewCell

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
        placeViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Use init(frame:)")
    }

    override func prepareForReuse() {
        disposeBag = DisposeBag()
        adImageView.image = nil
        titleLabel.text = nil
        subtitleLabel.text = nil
        callToActionButton.setTitle(nil, for: .normal)
        adChoicesView.nativeAd = nil
        if let adViewModel = viewModel,
            case .facebook(let nativeAd) = adViewModel.adModel.type {
            nativeAd.unregisterView()
        }
        viewModel = nil
        super.prepareForReuse()
    }

    // MARK: - Public

    func bind(to viewModel: AdViewModel, with viewController: UIViewController?) {
        if case .facebook(let nativeAd) = viewModel.adModel.type {
            self.viewModel = viewModel
            adChoicesView.nativeAd = nativeAd
            nativeAd.registerView(forInteraction: self.contentView,
                                  with: viewController,
                                  withClickableViews: [callToActionButton])

            viewModel.title.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.titleTextAttributes) }
                .drive(titleLabel.rx.attributedText)
                .disposed(by: disposeBag)

            viewModel.subtitle.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "", attributes: FeedItemCellAttributes.subtitleTextAttributes) }
                .drive(subtitleLabel.rx.attributedText)
                .disposed(by: disposeBag)

            viewModel.callToAction.asDriver(onErrorJustReturn: nil)
                .map { NSAttributedString(string: $0 ?? "",
                                          attributes: FeedItemCellAttributes.callToActionTextAttributes) }
                .drive(callToActionButton.rx.attributedTitle(for: .normal))
                .disposed(by: disposeBag)

            viewModel.adImage.asDriver(onErrorJustReturn: nil)
                .drive(adImageView.rx.image)
                .disposed(by: disposeBag)
        } else {
            assertionFailure("Wrong ad type")
        }
    }

    // MARK: - Private

    private func configureViews() {
        roundCornersContainer.layer.cornerRadius = 4
        roundCornersContainer.layer.masksToBounds = true
        roundCornersContainer.backgroundColor = nil
        roundCornersContainer.translatesAutoresizingMaskIntoConstraints = false

        adImageView.translatesAutoresizingMaskIntoConstraints = false

        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.numberOfLines = 0

        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        subtitleLabel.numberOfLines = 0

        callToActionButton.translatesAutoresizingMaskIntoConstraints = false

        sponsoredLabel.translatesAutoresizingMaskIntoConstraints = false
        sponsoredLabel.textColor = UIColor(white: 188/255, alpha: 1.0)
        sponsoredLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        sponsoredLabel.text = "*SPONSORED*"
        adChoicesView.translatesAutoresizingMaskIntoConstraints = false
        adChoicesView.isBackgroundShown = false
    }

    private func placeViews() {
        roundCornersContainer.addSubview(adImageView)
        contentView.addSubview(roundCornersContainer)
        contentView.addSubview(titleLabel)
        contentView.addSubview(subtitleLabel)
        contentView.addSubview(callToActionButton)
        contentView.addSubview(sponsoredLabel)
        contentView.addSubview(adChoicesView)

        adImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
            make.size.equalTo(72)
        }

        roundCornersContainer.snp.makeConstraints { (make) in
            make.leading.equalToSuperview().offset(16)
            make.top.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(roundCornersContainer.snp.top)
            make.leading.equalTo(roundCornersContainer.snp.trailing).offset(16)
            make.trailing.equalToSuperview().inset(28)
        }
        
        subtitleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel.snp.leading)
            make.trailing.equalTo(titleLabel.snp.trailing)
            make.top.equalTo(titleLabel.snp.bottom)
        }

        callToActionButton.snp.makeConstraints { (make) in
            make.leading.equalTo(subtitleLabel)
            make.bottom.equalToSuperview().inset(8)
        }

        sponsoredLabel.snp.makeConstraints { (make) in
            make.leading.greaterThanOrEqualTo(callToActionButton.snp.trailing).inset(16)
            make.trailing.equalToSuperview().inset(16)
            make.centerY.equalTo(callToActionButton)
        }

        adChoicesView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.trailing.equalToSuperview()
            make.size.equalTo(CGSize(width: 20, height: 20))
        }
    }
}

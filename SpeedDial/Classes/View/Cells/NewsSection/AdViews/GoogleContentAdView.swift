//
//  GoogleContentAdView.swift
//  SpeedDial
//
//  Created by ib on 13/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import GoogleMobileAds

final class GoogleContentAdView: GADNativeContentAdView {

    // MARK: - UIProperties

    @IBOutlet weak var sponsoredLabel: UILabel! {
        didSet {
            // TODO: Change style
            sponsoredLabel.textColor = UIColor(white: 188/255, alpha: 1.0)
            sponsoredLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        }
    }

    @IBOutlet weak var roundCornerContainer: UIView! {
        didSet {
            roundCornerContainer.backgroundColor = nil
            roundCornerContainer.layer.masksToBounds = true
            roundCornerContainer.layer.cornerRadius = 4.0
        }
    }

    var titleLabel: UILabel {
        guard let titleLabel = headlineView as? UILabel else {
            fatalError("Headline view is not a label")
        }
        return titleLabel
    }

    var subtitleLabel: UILabel {
        guard let subtitleLabel = bodyView as? UILabel else {
            fatalError("Headline view is not a label")
        }
        return subtitleLabel
    }

    var adImageView: UIImageView {
        guard let imageView = self.imageView as? UIImageView else {
            fatalError("imageView is not UIImageView")
        }
        return imageView
    }

    var callToActionButton: UIButton {
        guard let callToActionButton = callToActionView as? UIButton else {
            fatalError("CallToActionView is not UIButton")
        }
        return callToActionButton
    }

    // MARK: - UIView

    override func awakeFromNib() {
        super.awakeFromNib()

        // TODO: Change style
        titleLabel.textColor = UIColor(red: 60/255, green: 60/255, blue: 70/255, alpha: 1.0)
        titleLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)

        subtitleLabel.textColor = UIColor(white: 162/255, alpha: 1.0)
        subtitleLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)

        callToActionButton.setTitleColor(UIColor(red: 89/255, green: 165/255, blue: 255/255, alpha: 1.0),
                                         for: .normal)
        callToActionButton.titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .bold)
    }
}

//
//  FeedItemCollectionViewCell.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum FeedItemCellAttributes {
    static let thumbnailCornerRadius = CGFloat(4)

    static let titleTextAttributes: [NSAttributedStringKey: Any] = [
        .font: UIFont.systemFont(ofSize: 12, weight: .bold),
         .foregroundColor: UIColor(red: 60/255, green: 60/255, blue: 70/255, alpha: 1.0),
         .kern: NSNumber(value: 0.3),
         .paragraphStyle: NSParagraphStyle.paragraphStyle(lineSpacing: 4)
    ]
    static let subtitleTextAttributes: [NSAttributedStringKey: Any] = [
        .font: UIFont.systemFont(ofSize: 12, weight: .regular),
        .foregroundColor: UIColor(red: 60/255, green: 60/255, blue: 70/255, alpha: 1.0),
        .kern: NSNumber(value: 0.3),
        .paragraphStyle: NSParagraphStyle.paragraphStyle(lineSpacing: 4)
    ]
    static let callToActionTextAttributes: [NSAttributedStringKey: Any] = [
        .font: UIFont.systemFont(ofSize: 12, weight: .bold),
        .foregroundColor: UIColor(red: 89/255, green: 165/255, blue: 255/255, alpha: 1.0),
        .kern: NSNumber(value: 0.3),
        .paragraphStyle: NSParagraphStyle.paragraphStyle(lineSpacing: 3)
    ]
}

final class FeedItemCollectionViewCell: UICollectionViewCell, BaseCollectionViewCell {

    // MARK: - UIProperties

    private let roundCornerView = UIView()
    private let thumbnailImageView = UIImageView()
    private let titleLabel = UILabel()
    private let sourceLabel = UILabel()
    private let dateLabel = UILabel()

    // MARK: - Properties

    private var disposeBag = DisposeBag()
    var viewModel: NewsViewModel?

    // MARK: - Init

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
        placeViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("User init(frame: ")
    }

    override func prepareForReuse() {
        disposeBag = DisposeBag()
        titleLabel.attributedText = nil
        thumbnailImageView.image = nil
        sourceLabel.text = nil
        dateLabel.text = nil

        super.prepareForReuse()
    }

    // MARK: - Public

    func bind(to viewModel: NewsViewModel) {
        self.viewModel = viewModel

        viewModel.newsThumbnail
            .asDriver(onErrorJustReturn: nil)
            .drive(thumbnailImageView.rx.image)
            .disposed(by: disposeBag)

        Driver.combineLatest(viewModel.title.asDriver(onErrorJustReturn: ""),
                             viewModel.subtitle.asDriver(onErrorJustReturn: nil))
            .map { (title, subtitle) -> NSAttributedString in
                let attributedTitle = NSMutableAttributedString(string: title,
                                                                attributes: FeedItemCellAttributes.titleTextAttributes)
                if let subtitle = subtitle {

                    let attributedSubtitle = NSAttributedString(string: " - \(subtitle)",
                        attributes: FeedItemCellAttributes.subtitleTextAttributes)
                    attributedTitle.append(attributedSubtitle)
                }

                return attributedTitle
            }
            .drive(titleLabel.rx.attributedText)
            .disposed(by: disposeBag)

        viewModel.source
            .asDriver(onErrorJustReturn: nil)
            .drive(sourceLabel.rx.text)
            .disposed(by: disposeBag)

        viewModel.date
            .asDriver(onErrorJustReturn: nil)
            .drive(dateLabel.rx.text)
            .disposed(by: disposeBag)
    }

    // MARK: - Private

    private func configureViews() {
        roundCornerView.layer.cornerRadius = 4.0
        roundCornerView.layer.masksToBounds = true

        thumbnailImageView.contentMode = .scaleAspectFill

        titleLabel.numberOfLines = 2
        titleLabel.lineBreakMode = .byTruncatingTail

        sourceLabel.textColor = UIColor(white: 188/255, alpha: 1.0)
        sourceLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        sourceLabel.textAlignment = .left

        dateLabel.textColor = UIColor(white: 188/255, alpha: 1.0)
        dateLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        dateLabel.textAlignment = .right
        dateLabel.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 999), for: .horizontal)
        dateLabel.setContentHuggingPriority(UILayoutPriority(rawValue: 999), for: .horizontal)
    }

    private func placeViews() {
        roundCornerView.translatesAutoresizingMaskIntoConstraints = false
        thumbnailImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        sourceLabel.translatesAutoresizingMaskIntoConstraints = false
        dateLabel.translatesAutoresizingMaskIntoConstraints = false

        roundCornerView.addSubview(thumbnailImageView)
        contentView.addSubview(roundCornerView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(sourceLabel)
        contentView.addSubview(dateLabel)

        thumbnailImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        roundCornerView.snp.makeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalTo(contentView.snp.height)
            make.leading.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(roundCornerView.snp.trailing).offset(16)
            make.top.equalTo(roundCornerView.snp.top)
            make.trailing.equalToSuperview().inset(16)
        }

        sourceLabel.snp.makeConstraints { (make) in
            make.leading.equalTo(titleLabel.snp.leading)
            make.bottom.equalToSuperview()
        }

        dateLabel.snp.makeConstraints { (make) in
            make.trailing.equalToSuperview().inset(16)
            make.bottom.equalToSuperview()
            make.leading.equalTo(sourceLabel.snp.trailing)
        }
    }
}

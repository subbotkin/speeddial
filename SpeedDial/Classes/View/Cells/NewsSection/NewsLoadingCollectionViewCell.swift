//
//  NewsLoadingCollectionViewCell.swift
//  SpeedDial
//
//  Created by ib on 17/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class NewsLoadingCollectionViewCell: UICollectionViewCell, BaseCollectionViewCell {

    // MARK: - UIProperties

    private let roundCornerView = UIView()
    private let titleView = UIView()
    private let subtitleView = UIView()

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
        placeViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private

    private func configureViews() {
        roundCornerView.layer.cornerRadius = 4.0
        roundCornerView.layer.masksToBounds = true
        roundCornerView.backgroundColor = UIColor(white: 240/255, alpha: 1.0)

        titleView.layer.cornerRadius = 4.0
        titleView.layer.masksToBounds = true
        titleView.backgroundColor = UIColor(white: 240/255, alpha: 1.0)

        subtitleView.layer.cornerRadius = 4.0
        subtitleView.layer.masksToBounds = true
        subtitleView.backgroundColor = UIColor(white: 240/255, alpha: 1.0)
    }

    private func placeViews() {
        roundCornerView.translatesAutoresizingMaskIntoConstraints = false
        titleView.translatesAutoresizingMaskIntoConstraints = false
        subtitleView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(roundCornerView)
        contentView.addSubview(titleView)
        contentView.addSubview(subtitleView)

        roundCornerView.snp.makeConstraints { (make) in
            make.height.equalToSuperview()
            make.width.equalTo(contentView.snp.height)
            make.leading.equalToSuperview().inset(16)
            make.centerY.equalToSuperview()
        }

        titleView.snp.makeConstraints { (make) in
            make.leading.equalTo(roundCornerView.snp.trailing).offset(28)
            make.height.equalTo(8)
            make.width.equalToSuperview().multipliedBy(0.45)
            make.top.equalTo(roundCornerView.snp.top)
        }

        subtitleView.snp.makeConstraints { (make) in
            make.top.equalTo(titleView.snp.bottom).offset(10)
            make.leading.equalTo(titleView)
            make.width.equalToSuperview().multipliedBy(0.375)
            make.height.equalTo(8)
        }
    }
}

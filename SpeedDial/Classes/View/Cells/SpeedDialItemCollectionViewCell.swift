//
//  SpeedDialBookmarkCollectionViewCellPhone.swift
//  SpeedDial
//
//  Created by ib on 31/10/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

final class SpeedDialItemCollectionViewCell: UICollectionViewCell, BaseCollectionViewCell {

    // MARK: - UIProperties

    private let roundCornersView = UIView()
    private let iconImageView = UIImageView()
    private let titleLabel = UILabel()

    // MARK: - Properties

    private var reuseDisposeBag = DisposeBag()
    var speedDialItem: SpeedDialItem?

    // MARK: - Cell Lifecycle

    override init(frame: CGRect) {
        super.init(frame: frame)
        configureViews()
        placeViews()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Use init(frame:)")
    }

    override func prepareForReuse() {
        reuseDisposeBag = DisposeBag()
        speedDialItem = nil
        titleLabel.text = nil
        iconImageView.image = nil
        super.prepareForReuse()
    }

    // MARK: - Public

    func bind(to speedDialItem: SpeedDialItem) {
        self.speedDialItem = speedDialItem

        speedDialItem.iconImage
            .asDriver(onErrorJustReturn: nil)
            .drive(iconImageView.rx.image)
            .disposed(by: reuseDisposeBag)

        speedDialItem.title
            .asDriver(onErrorJustReturn: nil)
            .drive(titleLabel.rx.text)
            .disposed(by: reuseDisposeBag)
    }

    // MARK: - Private

    private func configureViews() {
        roundCornersView.layer.cornerRadius = 4.0
        roundCornersView.layer.masksToBounds = true
        roundCornersView.layer.borderWidth = 1.0
        roundCornersView.layer.borderColor = UIColor(white: 151/255, alpha: 0.18).cgColor // TODO: Change Color

        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 1
        titleLabel.lineBreakMode = .byClipping
    }

    private func placeViews() {
        roundCornersView.translatesAutoresizingMaskIntoConstraints = false
        iconImageView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false

        roundCornersView.addSubview(iconImageView)
        contentView.addSubview(roundCornersView)
        contentView.addSubview(titleLabel)

        iconImageView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        roundCornersView.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.top.equalToSuperview()
            make.height.equalTo(contentView.snp.width)
        }

        titleLabel.snp.makeConstraints { (make) in
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalTo(roundCornersView.snp.bottom)
        }
    }
}

//
//  HeaderCollectionViewCell.swift
//  SpeedDial
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class SpeedDialHeaderView: BaseReusableView {

    private enum Constants {
        static let settingsButtonTopOffset = CGFloat(8)
    }

    // MARK: - Outlets
    @IBOutlet weak var themeImageView: UIImageView!
    @IBOutlet weak var settingsButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var roundCornerView: UIView! {
        didSet {
            let mask = CAShapeLayer()
            roundCornerView.layer.mask = mask
        }
    }
    @IBOutlet weak var settingsButton: UIButton!

    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        super.apply(layoutAttributes)
        guard let customAttributes = layoutAttributes as? SpeedDialLayoutAttributes else {
            return
        }
        let topOffset = customAttributes.safeAreaInsets.top + Constants.settingsButtonTopOffset
        guard settingsButtonTopConstraint.constant != topOffset else {
            return
        }
        settingsButtonTopConstraint.constant = topOffset
        layoutIfNeeded()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        guard let roundCornerMask = roundCornerView.layer.mask as? CAShapeLayer else {
            return
        }
        roundCornerMask.frame = roundCornerView.bounds
        let radius = min(roundCornerView.bounds.width, roundCornerMask.bounds.height)
        roundCornerMask.path = UIBezierPath(roundedRect: roundCornerMask.frame,
                                            cornerRadius: radius / 2).cgPath
    }

}

//
//  SpeedDialLayoutAttributes.swift
//  SpeedDial
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

class SpeedDialLayoutAttributes: UICollectionViewLayoutAttributes {

    // MARK: - Properties

    // MARK: - Life Cycle
    override func copy(with zone: NSZone?) -> Any {
        guard let copiedAttributes = super.copy(with: zone) as? SpeedDialLayoutAttributes else {
            return super.copy(with: zone)
        }
        
        return copiedAttributes
    }

    override func isEqual(_ object: Any?) -> Bool {
        guard let otherAttributes = object as? SpeedDialLayoutAttributes else {
            return false
        }

        return super.isEqual(object)
    }
}

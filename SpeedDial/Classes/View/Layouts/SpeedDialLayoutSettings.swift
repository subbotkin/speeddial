//
//  SpeedDialLayoutSettings.swift
//  SpeedDial
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

struct SpeedDialLayoutSettings {

    // Elements sizes
    var headerSize: CGSize?
    var bookmarkItemSize: CGSize?
    var frequentlyVisitedItemSize: CGSize?
    var newsItemSize: CGSize?
    var sectionsHeaderSize: CGSize?

    // Spacing
    var sectionInsets: UIEdgeInsets = .zero
    var minimumInterItemSpacing: CGFloat
    var minimumLineSpacing: CGFloat
    var sectionsLineSpacing: CGFloat
    var newsLineSpacing: CGFloat
}

extension SpeedDialLayoutSettings {
    init() {
        minimumLineSpacing = 0
        minimumInterItemSpacing = 0
        sectionsLineSpacing = 0
        newsLineSpacing = 0
    }
}

//
//  SpeedDialCollectionViewLayout.swift
//  AlohaBrowser
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 Aloha Mobile Ltd. All rights reserved.
//

import UIKit

final class SpeedDialCollectionViewLayout: UICollectionViewLayout, LayoutParametersProvider {

    enum CustomElementKind {
        static let headerKind = "SpeedDialHeaderKind"
        static let addressBarKind = "AddressBarKind"
    }

    override class var layoutAttributesClass: AnyClass {
        return SpeedDialLayoutAttributes.self
    }

    override var collectionViewContentSize: CGSize {
        return CGSize(width: collectionViewWidth, height: contentYCursor)
    }

    // MARK: - Properties

    private var layouts: [SpeedDialLayout] = []
    private var headerLayout: SpeedDialLayout?
    private var addressBarLayout: AddressBarLayout?

    var settings: SpeedDialLayoutSettings!
    var feedHeightCalculator: FeedHeightCalculator!
    fileprivate var oldBounds = CGRect.zero
    fileprivate var contentYCursor: CGFloat = 0
    fileprivate var visibleLayoutAttributes = [SpeedDialLayoutAttributes]()
    fileprivate var zIndex = 0

    fileprivate var collectionViewHeight: CGFloat {
        return collectionView?.frame.height ?? 0
    }

    fileprivate var collectionViewWidth: CGFloat {
        return collectionView?.frame.width ?? 0
    }

    var contentOffset: CGPoint {
        return collectionView?.contentOffset ?? .zero
    }

    var headerSize: CGSize {
        return settings.headerSize ?? .zero
    }

    var sectionHeaderSize: CGSize {
        return settings.sectionsHeaderSize ?? .zero
    }

    var sectionLineSpacing: CGFloat {
        return settings.sectionsLineSpacing
    }

    var sectionInsets: UIEdgeInsets {
        return settings.sectionInsets
    }

    var itemSize: CGSize {
        return settings.bookmarkItemSize ?? .zero
    }

    private(set) var interItemSpace: CGFloat = 0

    var itemLineSpacing: CGFloat {
        return settings.minimumLineSpacing
    }

    var fillableWidth: CGFloat {
        return collectionViewWidth - settings.sectionInsets.left - settings.sectionInsets.right
    }

    var newsSize: CGSize {
        return settings.newsItemSize ?? .zero
    }

    var newsLineSpacing: CGFloat {
        return settings.newsLineSpacing
    }

    private func calculatePreciseInterItemSpace() -> CGFloat {
        let numberOfColumnsRaw = ((fillableWidth + settings.minimumInterItemSpacing) /
            (itemSize.width + settings.minimumInterItemSpacing))
        let numberOfColumns = numberOfColumnsRaw.rounded(.down)
        let preciseInterItemSpace = (fillableWidth -
            (itemSize.width * numberOfColumns)) / (numberOfColumns - 1)
        return preciseInterItemSpace
    }
}

extension SpeedDialCollectionViewLayout {

    override public func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if oldBounds.size != newBounds.size {
            layouts.removeAll(keepingCapacity: true)
        }
        return true
    }

    override func invalidateLayout() {
        layouts.removeAll(keepingCapacity: true)
        super.invalidateLayout()
    }

    override func prepare() {
        guard let collectionView = collectionView,
            collectionView.numberOfSections > 0,
            layouts.isEmpty else {
                return
        }

        prepareCache()
        contentYCursor = 0
        interItemSpace = calculatePreciseInterItemSpace()
        zIndex = 0
        oldBounds = collectionView.bounds

        let numberOfItemsInFirstSection = collectionView.numberOfItems(inSection: 0)
        headerLayout?.prepare(with: &contentYCursor, zIndex: &zIndex, itemsCount: numberOfItemsInFirstSection)

        for section in 0 ..< collectionView.numberOfSections {
            for layout in layouts {
                guard layout.section == section else {
                    continue
                }
                let numberOfItems = collectionView.numberOfItems(inSection: section)
                layout.prepare(with: &contentYCursor, zIndex: &zIndex, itemsCount: numberOfItems)
            }
        }
        
        addressBarLayout?.prepare(with: &contentYCursor, zIndex: &zIndex, itemsCount: numberOfItemsInFirstSection)
    }

    private func prepareCache() {
        layouts.removeAll(keepingCapacity: true)
        headerLayout = HeaderLayout(supplementaryViewKind: CustomElementKind.headerKind,
                                    section: 0,
                                    parametersProvider: self)
        addressBarLayout = AddressBarLayout(supplementaryViewKind: CustomElementKind.addressBarKind,
                                            section: 1,
                                            parametersProvider: self)

        let bookmarksLayout = BookmarksLayout(supplementaryViewKind: UICollectionElementKindSectionHeader,
                                              section: 0,
                                              parametersProvider: self)

        let frequentlyVisitedLayout =
            FrequentlyVisitedLayout(supplementaryViewKind: UICollectionElementKindSectionHeader,
                                    section: 1,
                                    parametersProvider: self)

        let newsLayout = NewsLayout(supplementaryViewKind: UICollectionElementKindSectionHeader,
                                    section: 2,
                                    parametersProvider: self,
                                    heightCalculator: feedHeightCalculator)

        layouts = [
            bookmarksLayout,
            frequentlyVisitedLayout,
            newsLayout
        ]
    }
}

extension SpeedDialCollectionViewLayout {

    override public func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let layout = layouts[indexPath.section]
        let attribute = layout.attributes[indexPath.item]
        return attribute
    }

    override public func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        visibleLayoutAttributes.removeAll(keepingCapacity: true)
        if let headerLayout = headerLayout {
            visibleLayoutAttributes.append(contentsOf: headerLayout.layoutAttributes(in: rect))
        }
        if let addressBarLayout = addressBarLayout {
            visibleLayoutAttributes.append(contentsOf: addressBarLayout.layoutAttributes(in: rect))
        }
        for layout in layouts {
            visibleLayoutAttributes.append(contentsOf: layout.layoutAttributes(in: rect))
        }
        return visibleLayoutAttributes
    }

    override func layoutAttributesForSupplementaryView(ofKind elementKind: String,
                                                       at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        switch elementKind {
        case UICollectionElementKindSectionHeader:
            let layout = layouts[indexPath.section]
            return layout.headerAttributes
        case CustomElementKind.headerKind:
            return headerLayout?.headerAttributes
        case CustomElementKind.addressBarKind:
            return addressBarLayout?.headerAttributes
        default:
            fatalError("Unsupported element kind \(elementKind)")
        }
    }
}

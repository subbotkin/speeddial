//
//  AddressBarLayout.swift
//  SpeedDial
//
//  Created by Alexey Subbotkin on 28/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class AddressBarLayout: SpeedDialLayout {

    // MARK: - Properties

    weak var parametersProvider: LayoutParametersProvider?
    let section: Int
    let headerAttributes: SpeedDialLayoutAttributes
    private(set) var itemsAttributes: [SpeedDialLayoutAttributes]
    
    private let addressBarHeight: CGFloat = 70
    private let margin: CGFloat = 16

    // MARK: - Init

    init(supplementaryViewKind kind: String, section: Int, parametersProvider provider: LayoutParametersProvider) {
        self.section = section
        self.parametersProvider = provider
        headerAttributes = AddressBarSupplementaryViewLayoutAttributes(forSupplementaryViewOfKind: kind,
                                                                       with: IndexPath(item: 0, section: section))
        itemsAttributes = []
    }

    // MARK: - Computed properties

    private var offset: CGPoint {
        return parametersProvider?.contentOffset ?? .zero
    }

    private var headerSize: CGSize {
        return parametersProvider?.headerSize ?? .zero
    }
    
    private var addressBarFrame: CGRect {
        switch offset.y {
        case ...0: return CGRect(x: 0, y: 0, width: 100, height: addressBarHeight)
        case 0...: return CGRect(x: 0, y: 0, width: 150, height: addressBarHeight)
        default: return .zero
        }
    }

    // MARK: - SpeedDialLayout

    var attributes: [SpeedDialLayoutAttributes] {
        return itemsAttributes
    }

    func prepare(with yCursor: inout CGFloat, zIndex: inout Int, itemsCount: Int? = nil) {
        guard let itemsCount = itemsCount, itemsCount > 0 else {
            return
        }
        itemsAttributes.removeAll(keepingCapacity: true)
        headerAttributes.frame = addressBarFrame
        headerAttributes.zIndex = zIndex
        zIndex += 1
        itemsAttributes.append(headerAttributes)
    }

    func layoutAttributes(in rect: CGRect) -> [SpeedDialLayoutAttributes] {
        updateAttributes(with: offset, headerSize: headerSize)
        guard let headerAttributes = itemsAttributes.first,
            headerAttributes.frame.intersects(rect) else {
                return []
        }
        return [headerAttributes]
    }

    // MARK: - Private

    fileprivate func updateAttributes(with contentOffset: CGPoint, headerSize: CGSize) {
        let frame: CGRect
        let progress: CGFloat
        switch offset.y {
        case ...(headerSize.height - addressBarHeight / 2):
            progress = min(1, max(0, (offset.y / (headerSize.height - addressBarHeight / 2))))
            let margin = self.margin * (1 - progress)
            frame = CGRect(x: margin,
                           y: headerSize.height - addressBarHeight / 2,
                           width: headerSize.width - 2 * margin,
                           height: addressBarHeight)
        case (headerSize.height - addressBarHeight / 2)...:
            progress = 1
            frame = CGRect(x: 0, y: offset.y, width: headerSize.width, height: addressBarHeight)
        default: return frame = .zero
        }
        
        headerAttributes.frame = frame
        (headerAttributes as? AddressBarSupplementaryViewLayoutAttributes)?.progress = progress
    }
}

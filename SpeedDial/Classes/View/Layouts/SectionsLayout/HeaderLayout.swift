//
//  HeaderLayout.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class HeaderLayout: SpeedDialLayout {

    // MARK: - Properties

    weak var parametersProvider: LayoutParametersProvider?
    let section: Int
    let headerAttributes: SpeedDialLayoutAttributes
    private(set) var itemsAttributes: [SpeedDialLayoutAttributes]

    // MARK: - Init

    init(supplementaryViewKind kind: String, section: Int, parametersProvider provider: LayoutParametersProvider) {
        self.section = section
        self.parametersProvider = provider
        headerAttributes = SpeedDialLayoutAttributes(forSupplementaryViewOfKind: kind,
                                                     with: IndexPath(item: 0, section: section))
        itemsAttributes = []
    }

    // MARK: - Computed properties

    private var offset: CGPoint {
        return parametersProvider?.contentOffset ?? .zero
    }

    private var headerSize: CGSize {
        return parametersProvider?.headerSize ?? .zero
    }

    // MARK: - SpeedDialLayout

    var attributes: [SpeedDialLayoutAttributes] {
        return itemsAttributes
    }

    func prepare(with yCursor: inout CGFloat, zIndex: inout Int, itemsCount: Int? = nil) {
        guard let itemsCount = itemsCount, itemsCount > 0 else {
            return
        }
        itemsAttributes.removeAll(keepingCapacity: true)
        let origin = CGPoint(x: 0, y: yCursor)
        headerAttributes.frame = CGRect(origin: origin, size: headerSize)
        headerAttributes.zIndex = zIndex
        yCursor = headerAttributes.frame.maxY
        zIndex += 1
        itemsAttributes.append(headerAttributes)
    }

    func layoutAttributes(in rect: CGRect) -> [SpeedDialLayoutAttributes] {
        updateAttributes(with: offset)
        guard let headerAttributes = itemsAttributes.first,
            headerAttributes.frame.intersects(rect) else {
                return []
        }
        return [headerAttributes]
    }

    // MARK: - Private

    fileprivate func updateAttributes(with contentOffset: CGPoint) {
        if contentOffset.y < 0 {
            let updatedHeight = max(headerSize.height, headerSize.height - contentOffset.y)
            let newSize = CGSize(width: headerAttributes.frame.width, height: updatedHeight)
            let newOrigin = CGPoint(x: 0, y: contentOffset.y)
            headerAttributes.frame = CGRect(origin: newOrigin, size: newSize)
        }
    }
}

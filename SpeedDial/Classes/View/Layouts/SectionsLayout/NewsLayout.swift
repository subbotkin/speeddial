//
//  NewsLayout.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class NewsLayout: SpeedDialLayout {

    // MARK: - Proeprties

    weak var parametersProvider: LayoutParametersProvider?
    let section: Int
    let headerAttributes: SpeedDialLayoutAttributes
    
    private(set) var itemsAttributes: [SpeedDialLayoutAttributes]
    private let kind: String
    private var itemsHeightCalculator: FeedHeightCalculator!

    private var cursor: CGPoint = .zero

    // MARK: - Computed Properties

    private var sectionHeaderSize: CGSize {
        return parametersProvider?.sectionHeaderSize ?? .zero
    }

    private var sectionInsets: UIEdgeInsets {
        return parametersProvider?.sectionInsets ?? .zero
    }

    private var sectionLineSpacing: CGFloat {
        return parametersProvider?.sectionLineSpacing ?? 0
    }

    private var itemSize: CGSize {
        return parametersProvider?.newsSize ?? .zero
    }

    private var itemLineSpacing: CGFloat {
        return parametersProvider?.newsLineSpacing ?? 0
    }

    // MARK: - Init

    init(supplementaryViewKind kind: String,
         section: Int,
         parametersProvider provider: LayoutParametersProvider) {
        self.kind = kind
        self.section = section
        self.headerAttributes = SpeedDialLayoutAttributes(forSupplementaryViewOfKind: kind,
                                                          with: IndexPath(item: 0, section: section))

        self.parametersProvider = provider
        itemsAttributes = []
    }

    convenience init(supplementaryViewKind kind: String,
                     section: Int,
                     parametersProvider provider: LayoutParametersProvider,
                     heightCalculator: FeedHeightCalculator) {
        self.init(supplementaryViewKind: kind, section: section, parametersProvider: provider)
        itemsHeightCalculator = heightCalculator
        itemsAttributes = []
    }

    // MARK: - SpeedDial Layout

    var attributes: [SpeedDialLayoutAttributes] {
        return itemsAttributes
    }

    func prepare(with yCursor: inout CGFloat, zIndex: inout Int, itemsCount: Int?) {
        guard let itemsCount = itemsCount, itemsCount > 0 else {
            return
        }

        itemsAttributes.removeAll(keepingCapacity: true)
        prepareHeaderLayout(yPosition: yCursor, zIndex: &zIndex)
        for item in 0..<itemsCount {
            let cellIndexPath = IndexPath(item: item, section: section)
            let isLastCell = item == itemsCount - 1
            itemsAttributes.append(itemAttributes(with: cellIndexPath, zIndex: &zIndex, isLastCell: isLastCell))
        }
        itemsAttributes.append(headerAttributes)
        yCursor = cursor.y
    }

    func layoutAttributes(in rect: CGRect) -> [SpeedDialLayoutAttributes] {
        return itemsAttributes.filter { $0.frame.intersects(rect) }
    }

    // MARK: - Private

    private func prepareHeaderLayout(yPosition: CGFloat, zIndex: inout Int) {
        let xPos = sectionInsets.left
        let yPos = sectionLineSpacing + yPosition
        let origin = CGPoint(x: xPos, y: yPos)
        headerAttributes.frame = CGRect(origin: origin, size: sectionHeaderSize)
        headerAttributes.zIndex = zIndex
        zIndex += 1
        cursor = CGPoint(x: 0, y: headerAttributes.frame.maxY + 14)
    }

    private func itemAttributes(with indexPath: IndexPath,
                                zIndex: inout Int,
                                isLastCell: Bool) -> SpeedDialLayoutAttributes {
        let attributes = SpeedDialLayoutAttributes(forCellWith: indexPath)
        let feedItemSize = itemsHeightCalculator.sizeForNews(at: indexPath.item, itemSize: itemSize)
        attributes.frame = CGRect(origin: cursor, size: feedItemSize)
        attributes.zIndex = zIndex
        zIndex += 1
        cursor.y = attributes.frame.maxY + itemLineSpacing
        return attributes
    }
}

//
//  BookmarksLayout.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

final class BookmarksLayout: SpeedDialLayout {

    // MARK: - Proeprties
    
    weak var parametersProvider: LayoutParametersProvider?
    let section: Int
    let headerAttributes: SpeedDialLayoutAttributes
    private(set) var itemsAttributes: [SpeedDialLayoutAttributes]
    private let kind: String

    private var cursor: CGPoint = .zero

    // MARK: - Computed Properties

    private var sectionInsets: UIEdgeInsets {
        return parametersProvider?.sectionInsets ?? .zero
    }

    private var sectionLineSpacing: CGFloat {
        return parametersProvider?.sectionLineSpacing ?? 0
    }

    private var itemSize: CGSize {
        return parametersProvider?.itemSize ?? .zero
    }

    private var interItemSpace: CGFloat {
        return parametersProvider?.interItemSpace ?? 0
    }

    private var itemLineSpacing: CGFloat {
        return parametersProvider?.itemLineSpacing ?? 0
    }

    private var fillableWidth: CGFloat {
        return parametersProvider?.fillableWidth ?? 0
    }

    // MARK: - Init

    init(supplementaryViewKind kind: String,
         section: Int,
         parametersProvider provider: LayoutParametersProvider) {
        self.kind = kind
        self.section = section
        self.headerAttributes = SpeedDialLayoutAttributes(forSupplementaryViewOfKind: kind,
                                                          with: IndexPath(item: 0, section: section))

        self.parametersProvider = provider
        itemsAttributes = []
    }

    // MARK: - SpeedDial Layout

    var attributes: [SpeedDialLayoutAttributes] {
        return itemsAttributes
    }

    func prepare(with yCursor: inout CGFloat, zIndex: inout Int, itemsCount: Int?) {
        guard let itemsCount = itemsCount, itemsCount > 0 else {
            return
        }

        itemsAttributes.removeAll(keepingCapacity: true)
        prepareHeaderLayout(yPosition: yCursor, zIndex: &zIndex)
        for item in 0..<itemsCount {
            let cellIndexPath = IndexPath(item: item, section: section)
            let isLastCell = item == itemsCount - 1
            itemsAttributes.append(itemAttributes(with: cellIndexPath, zIndex: &zIndex, isLastCell: isLastCell))
        }
        itemsAttributes.append(headerAttributes)
        yCursor = cursor.y
    }

    func layoutAttributes(in rect: CGRect) -> [SpeedDialLayoutAttributes] {
        return itemsAttributes.filter { $0.frame.intersects(rect) }
    }

    // MARK: - Private

    private func prepareHeaderLayout(yPosition: CGFloat, zIndex: inout Int) {
        let xPos = sectionInsets.left
        let yPos = sectionLineSpacing + yPosition
        let origin = CGPoint(x: xPos, y: yPos)
        headerAttributes.frame = CGRect(origin: origin, size: .zero)
        headerAttributes.zIndex = zIndex
        zIndex += 1
        cursor = CGPoint(x: headerAttributes.frame.minX, y: headerAttributes.frame.maxY)
    }

    private func itemAttributes(with indexPath: IndexPath,
                                zIndex: inout Int,
                                isLastCell: Bool) -> SpeedDialLayoutAttributes {
        let attributes = SpeedDialLayoutAttributes(forCellWith: indexPath)

        attributes.frame = CGRect(
            x: cursor.x,
            y: cursor.y,
            width: itemSize.width,
            height: itemSize.height
        )
        attributes.zIndex = zIndex
        zIndex += 1

        let nextXPosition = cursor.x + interItemSpace + itemSize.width

        if nextXPosition >= fillableWidth {
            cursor.x = sectionInsets.left
            cursor.y = attributes.frame.maxY + itemLineSpacing
        } else if !isLastCell {
            cursor.x = nextXPosition
        } else {
            cursor.y = attributes.frame.maxY
            cursor.x = sectionInsets.left
        }

        return attributes
    }
}

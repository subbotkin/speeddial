//
//  LayoutParametersProvider.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

protocol LayoutParametersProvider: class {

    // Big header parameters
    var contentOffset: CGPoint { get }
    var headerSize: CGSize { get }

    // Sections Parameters
    var sectionHeaderSize: CGSize { get }
    var sectionLineSpacing: CGFloat { get }
    var sectionInsets: UIEdgeInsets { get }
    var itemSize: CGSize { get }
    var interItemSpace: CGFloat { get }
    var itemLineSpacing: CGFloat { get }
    var fillableWidth: CGFloat { get }

    // MARK: - News Parameters
    var newsSize: CGSize { get }
    var newsLineSpacing: CGFloat { get }
}

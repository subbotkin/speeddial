//
//  SpeedDialLayout.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

protocol SpeedDialLayout {
    init(supplementaryViewKind kind: String, section: Int, parametersProvider provider: LayoutParametersProvider)
    var section: Int { get }
    var headerAttributes: SpeedDialLayoutAttributes { get }
    var parametersProvider: LayoutParametersProvider? { get set }
    var attributes: [SpeedDialLayoutAttributes] { get }
    func prepare(with yCursor: inout CGFloat, zIndex: inout Int, itemsCount: Int?)
    func layoutAttributes(in rect: CGRect) -> [SpeedDialLayoutAttributes]
}

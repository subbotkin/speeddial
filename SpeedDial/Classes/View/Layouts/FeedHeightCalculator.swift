//
//  FeedHeightCalculator.swift
//  SpeedDial
//
//  Created by ib on 02/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

protocol FeedProvider {
    func feedItem(at index: Int) -> FeedItem?
}

final class FeedHeightCalculator {

    // MARK: - Properties

    // DI
    let feedProvider: FeedProvider
    private var adItemCache: [Int: CGSize] = [:]

    // MARK: - Init

    init(feedProvider: FeedProvider) {
        self.feedProvider = feedProvider
    }

    // MARK: - Public

    func invalidate() {
        adItemCache.removeAll()
    }

    func sizeForNews(at index: Int, itemSize: CGSize) -> CGSize {
        guard let feedItem = feedProvider.feedItem(at: index) else {
            return .zero
        }
        switch feedItem {
        case .news:
            return itemSize
        case .loading:
            return itemSize
        case .advertisment(let adViewModel):
            if let cachedSize = adItemCache[index] {
                return cachedSize
            } else {
                // No item in cache, calculate and save in cache
                let contentWidth = itemSize.width - AdCellAttributes.textContentHorizontalInsets
                let titleHeight = adViewModel.adModel.title
                    .height(withConstrainedWidth: contentWidth,
                            attributes: FeedItemCellAttributes.titleTextAttributes)

                let subtitleHeight = adViewModel.adModel.subtitle
                    .height(withConstrainedWidth: contentWidth,
                            attributes: FeedItemCellAttributes.subtitleTextAttributes)
                let callToActionHeight = adViewModel.adModel.callToAction
                    .height(withConstrainedWidth: contentWidth,
                            attributes: FeedItemCellAttributes.callToActionTextAttributes)
                var height = titleHeight + subtitleHeight + callToActionHeight +
                    AdCellAttributes.callToActionVerticalInsets

                if height > 0 && height < 80 {
                    height = 80
                }

                let size = CGSize(width: itemSize.width, height: height)
                adItemCache[index] = size
                return size
            }
        }
    }
}

extension Optional where Wrapped == String {
    func height(withConstrainedWidth width: CGFloat, attributes: [NSAttributedStringKey: Any]) -> CGFloat {
        switch self {
        case .none:
            return 0
        case .some(let string):
            return string.height(withConstrainedWidth: width, attributes: attributes)
        }
    }

    func width(withConstraintedHeight height: CGFloat, attributes: [NSAttributedStringKey: Any]) -> CGFloat {
        switch self {
        case .none:
            return 0
        case .some(let string):
            return string.width(withConstraintedHeight: height, attributes: attributes)
        }
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, attributes: [NSAttributedStringKey: Any]) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: attributes,
                                            context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstraintedHeight height: CGFloat, attributes: [NSAttributedStringKey: Any]) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: .usesLineFragmentOrigin,
                                            attributes: attributes,
                                            context: nil)

        return ceil(boundingBox.width)
    }
}

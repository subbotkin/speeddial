//
//  Tile.swift
//  SpeedDial
//
//  Created by ib on 21/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class Tile {

    enum TileType: String {
        case info = "info"
        case advertisment = "ad"
    }

    private(set) var identifier: Int
    private(set) var isModal: Bool
    private var imageUrlString: String
    private var adUrlString: String
    private var typeString: String

    var imageUrl: URL? {
        return URL(string: imageUrlString)
    }

    var adUrl: URL? {
        return URL(string: adUrlString)
    }

    var tileType: TileType {
        guard let tileType = TileType(rawValue: typeString) else {
            assertionFailure("No such type \(typeString)")
            return .info
        }
        return tileType
    }

    init(identifier: Int, imageUrl: String, adUrl: String, isModal: Bool, type: String) {
        self.identifier = identifier
        self.imageUrlString = imageUrl
        self.adUrlString = adUrl
        self.isModal = isModal
        self.typeString = type
    }
}

extension Tile: Decodable {
    private enum JSONKeys: String, CodingKey {
        case imageUrl = "img"
        case adUrl = "url"
        case isModal = "modal"
        case type = "type"
        case identifier = "id"
    }

    convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: JSONKeys.self)
        let identifier = try container.decode(Int.self, forKey: .identifier)
        let adUrl = try container.decode(String.self, forKey: .adUrl)
        let imageUrl = try container.decode(String.self, forKey: .imageUrl)
        let isModal = try container.decode(Bool.self, forKey: .isModal)
        let type = try container.decode(String.self, forKey: .type)

        self.init(identifier: identifier, imageUrl: imageUrl, adUrl: adUrl, isModal: isModal, type: type)
    }
}

extension Tile {
    static func == (lhs: Tile, rhs: Tile) -> Bool {
        return lhs.identifier == rhs.identifier
    }
}

extension Tile: CustomStringConvertible {
    var description: String {
        return """
        id: \(identifier)
        """
    }
}

//
//  NewsModel.swift
//  SpeedDial
//
//  Created by ib on 08/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class News: Object {
    @objc let tempId = NSUUID().uuidString
    @objc dynamic private(set) var title: String
    @objc dynamic private(set) var imageUrlString: String?
    @objc dynamic private(set) var urlString: String?
    @objc dynamic private(set) var subtitle: String?
    @objc dynamic private(set) var date: Date
    @objc dynamic private(set) var source: String

    var imageUrl: URL? {
        return URL(string: imageUrlString ?? "")
    }

    var url: URL? {
        return URL(string: urlString ?? "")
    }

    init(title: String, subtitle: String?, imageUrlString: String?, urlString: String?, date: Date, source: String) {
        self.title = title
        self.subtitle = subtitle
        self.imageUrlString = imageUrlString
        self.urlString = urlString
        self.date = date
        self.source = source
        super.init()
    }

    required init() {
        self.title = ""
        self.date = Date()
        self.source = ""
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        self.title = ""
        self.date = Date()
        self.source = ""
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        self.title = ""
        self.date = Date()
        self.source = ""
        super.init(realm: realm, schema: schema)
    }

    override static func ignoredProperties() -> [String] {
        return ["tempId"]
    }
}

extension News: Decodable {
    private enum JSONKeys: String, CodingKey {
        case title = "plainTitle"
        case subtitle = "plainSubtitle"
        case imageUrl = "img"
        case url = "url"
        case date = "date"
        case source = "source"
    }

    convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: JSONKeys.self)
        let title = try container.decode(String.self, forKey: .title)
        let subtitle = try container.decode(String?.self, forKey: .subtitle)
        let imageUrlString = try container.decode(String.self, forKey: .imageUrl)
        let url = try container.decode(String.self, forKey: .url)
        let date = try container.decode(Date.self, forKey: .date)
        let source = try container.decode(String.self, forKey: .source)

        self.init(title: title,
                  subtitle: subtitle,
                  imageUrlString: imageUrlString,
                  urlString: url,
                  date: date,
                  source: source)
    }
}

extension News {
    static func == (lhs: News, rhs: News) -> Bool {
        return lhs.tempId == rhs.tempId
    }
}

extension News {
    override var debugDescription: String {
        return """
        title: \(self.title)
        subtitle: \(self.subtitle ?? "Empty Subtitle")
        imageUrl: \(self.imageUrlString ?? "Empty URL")
        url: \(self.urlString ?? "Empty url")
        date: \(self.date)
        source: \(self.source)
        """
    }
}

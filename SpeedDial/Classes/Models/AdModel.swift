//
//  AdModel.swift
//  SpeedDial
//
//  Created by ib on 09/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import class FBAudienceNetwork.FBNativeAd
import class GoogleMobileAds.GADNativeContentAd
import class GoogleMobileAds.GADNativeAppInstallAd

enum AdType {
    case googleContent(GADNativeContentAd)
    case googleAppInstall(GADNativeAppInstallAd)
    case facebook(FBNativeAd)
}

extension AdType: Equatable {
    static func == (lhs: AdType, rhs: AdType) -> Bool {
        switch (lhs, rhs) {
        case (.googleContent, .googleContent): return true
        case (.googleAppInstall, .googleAppInstall): return true
        case (.facebook, .facebook): return true
        default: return false
        }
    }
}

struct AdModel {
    let type: AdType

    let id: String = NSUUID().uuidString
    let title: String?
    let subtitle: String?
    let callToAction: String?
    let iconUrl: URL?
}

extension AdModel: Equatable {
    static func == (lhs: AdModel, rhs: AdModel) -> Bool {
        return lhs.id == rhs.id
    }
}

// Support Facebook advertisment
extension AdModel {

    init(fbAd: FBNativeAd) {
        self.type = .facebook(fbAd)

        self.title = fbAd.title
        self.subtitle = fbAd.subtitle
        self.callToAction = fbAd.callToAction
        self.iconUrl = fbAd.icon?.url
    }
}

// Support Google Ad
extension AdModel {
    init(googleContentAd: GADNativeContentAd) {
        self.type = .googleContent(googleContentAd)
        self.title = googleContentAd.headline
        self.subtitle = googleContentAd.body
        self.callToAction = googleContentAd.callToAction
        self.iconUrl = googleContentAd.logo?.imageURL
    }

    init(googleAppInstallAd: GADNativeAppInstallAd) {
        self.type = .googleAppInstall(googleAppInstallAd)
        self.title = googleAppInstallAd.headline
        self.subtitle = googleAppInstallAd.body
        self.callToAction = googleAppInstallAd.callToAction
        self.iconUrl = googleAppInstallAd.icon?.imageURL
    }
}

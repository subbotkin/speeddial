//
//  BaseCollectionViewReusableView.swift
//  SpeedDial
//
//  Created by ib on 20/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import UIKit

protocol BaseCollectionReusableView {
    static var identifier: String { get }
    static var nib: UINib? { get }
}

extension BaseCollectionReusableView {
    static var identifier: String {
        return String(describing: self)
    }
    static var nib: UINib? {
        let nibName = String(describing: self)
        guard Bundle.main.path(forResource: nibName, ofType: "nib") != nil else {
            return nil
        }
        return UINib.init(nibName: nibName, bundle: nil)
    }
}

extension UICollectionView {

    func register<SupplementaryView: UICollectionReusableView>(
        viewType: SupplementaryView.Type,
        withKind kind: String) where SupplementaryView: BaseCollectionReusableView {
        if let nib = viewType.nib {
            register(nib, forSupplementaryViewOfKind: kind, withReuseIdentifier: viewType.identifier)
        } else {
            register(viewType, forSupplementaryViewOfKind: kind, withReuseIdentifier: viewType.identifier)
        }
    }

    func dequeueReusableSupplementaryView<ViewType: UICollectionReusableView>(
        _ viewType: ViewType.Type,
        ofKind kind: String,
        for indexPath: IndexPath
        ) -> ViewType where ViewType: BaseCollectionReusableView {
        let dequeuedView = dequeueReusableSupplementaryView(ofKind: kind,
                                                            withReuseIdentifier: ViewType.identifier,
                                                            for: indexPath)
        guard let typedView = dequeuedView as? ViewType else {
            fatalError("""
                Wrong dequed view type \(String(describing: dequeuedView.self)) identifier \(viewType.identifier)
                """)
        }
        return typedView
    }

}

//
//  BaseCollectionViewCell.swift
//  SpeedDial
//
//  Created by ib on 20/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

protocol BaseCollectionViewCell {
    static var identifier: String { get }
    static var nib: UINib? { get }
}

extension BaseCollectionViewCell {
    static var identifier: String {
        return String(describing: self)
    }

    static var nib: UINib? {
        let nibName = String(describing: self)
        guard Bundle.main.path(forResource: nibName, ofType: "nib") != nil else {
            return nil
        }
        return UINib(nibName: nibName, bundle: nil)
    }
}

extension UICollectionView {

    func register<Cell: UICollectionViewCell>(cellType: Cell.Type) where Cell: BaseCollectionViewCell {
        if let nib = cellType.nib {
            register(nib, forCellWithReuseIdentifier: cellType.identifier)
        } else {
            register(cellType, forCellWithReuseIdentifier: cellType.identifier)
        }
    }

    func dequeueCell<Cell: UICollectionViewCell>(_ cell: Cell.Type,
                                                 for indexPath: IndexPath) -> Cell where Cell: BaseCollectionViewCell {
        let dequeuedCell = dequeueReusableCell(withReuseIdentifier: cell.identifier, for: indexPath)
        guard let typedCell = dequeuedCell as? Cell else {
            fatalError("Wrong cell type \(String(describing: dequeuedCell.self)) for identifier \(cell.identifier)")
        }
        return typedCell
    }
}

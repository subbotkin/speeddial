//
//  NSParagraphStyle+Ext.swift
//  SpeedDial
//
//  Created by ib on 16/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

extension NSParagraphStyle {
    class func paragraphStyle(lineSpacing: CGFloat) -> NSParagraphStyle {
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineSpacing = lineSpacing
        return paragraph
    }
}

//
//  Result.swift
//  SpeedDial
//
//  Created by ib on 09/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}

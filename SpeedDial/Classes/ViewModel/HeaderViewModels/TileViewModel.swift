//
//  TileViewModel.swift
//  SpeedDial
//
//  Created by ib on 21/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxDataSources
import Action

final class TileViewModel {

    // MARK: - Properties

    private let imageService: ImageService
    private let disposeBag = DisposeBag()
    private let _tileImageSubject: ReplaySubject<UIImage?> = ReplaySubject.create(bufferSize: 1)

    // MARK: - Input

    let tile: Tile
    let onCloseButtonAction: Action<Tile, Tile>

    // MARK: - Output

    let infoIcon: Driver<UIImage?>
    var tileImage: Driver<UIImage?> {
        return _tileImageSubject.asDriver(onErrorJustReturn: nil)
    }

    init(tile: Tile, imageService: ImageService, closeButtonAction: Action<Tile, Tile>) {
        self.tile = tile
        self.imageService = imageService
        self.onCloseButtonAction = closeButtonAction

        infoIcon = Driver.just(tile.tileType).map { $0.infoImage }

        if let imageUrl = tile.imageUrl {
            imageService.imageFromURL(imageUrl)
                .map { downloadableImage -> UIImage? in
                    switch downloadableImage {
                    case .noImage: return nil
                    case .content(image: let image): return image
                    }
                }
                .bind(to: _tileImageSubject)
                .disposed(by: disposeBag)
        }
    }
}

extension TileViewModel: IdentifiableType {
    typealias Identity = String
    var identity: String {
        return "\(tile.identifier)"
    }
}

extension TileViewModel: Equatable {
    static func == (lhs: TileViewModel, rhs: TileViewModel) -> Bool {
        return lhs.tile == rhs.tile
    }
}

private extension Tile.TileType {
    var infoImage: UIImage {
        switch self {
        case .info: return #imageLiteral(resourceName: "infoTile")
        case .advertisment: return #imageLiteral(resourceName: "tileAd")
        }
    }
}

//
//  SpeedDialHeaderViewModel.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt
import RxFeedback
import Action
import RxDataSources

typealias TilesSection = AnimatableSectionModel<String, TileViewModel>

final class SpeedDialHeaderViewModel {

    private let disposeBag = DisposeBag()

    // MARK: - Input

    var openSettings: (() -> Void)?
    var onTileTap: ((ModalTileViewModel) -> Void)?

    // MARK: - Output

    let themeImage: Observable<UIImage?>
    let tiles: BehaviorSubject<[TilesSection]> = BehaviorSubject(value: [])

    // MARK: - Actions

    lazy var onSettingsTapAction: CocoaAction = { [weak self] in
        return CocoaAction {
            self?.openSettings?()
            return .empty()
        }
    }()

    lazy var onTileTapAction: Action<Tile, Void> = { [ weak self] in
        return Action { tile in
            guard let strongSelf = self else {
                return .empty()
            }
            if tile.isModal {
                let modalTileViewModel = ModalTileViewModel(tile: tile, imageService: strongSelf.imageService)
                strongSelf.onTileTap?(modalTileViewModel)
            } else {
                // TODO: Tap on non modal tile
            }
            return .empty()
        }
    }()

    lazy var onTileCloseAction: Action<Tile, Tile> = {
        return Action { tile in
            return .just(tile)
        }
    }()

    // MARK: - Properties

    private let imageService: ImageService
    private let tilesService: TilesService

    init(imageService: ImageService, tilesService: TilesService) {
        self.imageService = imageService
        self.tilesService = tilesService
        themeImage = Observable.just(#imageLiteral(resourceName: "aloha"))
        bindingOutput()
    }

    // MARK: - Private

    private func bindingOutput() {
        let bindUI: (Driver<TilesState>) -> Signal<TilesStateCommand> = bind(self) { viewModel, state in
            let subscriptions: [Disposable] = [
                state.map { $0.loadedTiles }
                    .map { $0.map { TileViewModel(tile: $0,
                                                  imageService: viewModel.imageService,
                                                  closeButtonAction: viewModel.onTileCloseAction)} }
                    .map { [TilesSection(model: "", items: $0)] }
                    .drive(viewModel.tiles),
                // TODO: Save new offset and showed tiles in storage
                state.map { $0.offset }.distinctUntilChanged(==)
                    .drive(onNext: { print("new Offset \($0)")}),
                state.map { $0.showingTilesIds }.distinctUntilChanged(==)
                    .drive(onNext: {print("Showing tiles ids \($0)")})
            ]
            let events: [Signal<TilesStateCommand>] = [
                viewModel.onTileCloseAction.elements
                    .asSignal(onErrorSignalWith: .empty())
                    .map(TilesStateCommand.closeTile),
                Signal.never() // TODO: React to reload tiles
            ]
            return Bindings(subscriptions: subscriptions, events: events)
        }

        Driver.system(
            initialState: TilesState(offset: nil, showingTilesIds: []),
            reduce: TilesState.reduce,
            feedback:
            bindUI,
            react(query: { $0.loadTiles },
                  effects: { [weak self] query in
                    guard let strongSelf = self else {
                        return .empty()
                    }
                    return strongSelf.tilesService
                        .fetchTiles(with: query.offset, showingTiles: query.showed)
                        .asSignal(onErrorJustReturn: .failure(.networkError))
                        .map(TilesStateCommand.tilesReceived)
            })
            )
            .drive()
            .disposed(by: disposeBag)
    }
}

//
//  TilesState.swift
//  SpeedDial
//
//  Created by ib on 23/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation

enum TilesStateCommand {
    case tilesReceived(TilesResponse)
    case closeTile(Tile)
}

struct TilesState {
    // control
    
    private var shouldLoadTiles: Bool
    private var failure: TilesServiceError?

    var offset: String?
    var showingTilesIds: [Int]
    var loadedTiles: [Tile]

    init(offset: String?, showingTilesIds: [Int]) {
        self.offset = offset
        self.showingTilesIds = showingTilesIds

        shouldLoadTiles = true
        loadedTiles = []
    }
}

extension TilesState {
    var loadTiles: (showed: [Int], offset: String?)? {
        return shouldLoadTiles ? (showingTilesIds, offset) : nil
    }
}

extension TilesState {
    static func reduce(state: TilesState, command: TilesStateCommand) -> TilesState {
        var newState = state
        switch command {
        case .closeTile(let tile):
            guard let deleteTileIndex = newState.loadedTiles.index(where: {$0 == tile }) else {
                return newState
            }
            var newTiles = newState.loadedTiles
            newTiles.remove(at: deleteTileIndex)
            newState.loadedTiles = newTiles
            newState.showingTilesIds = newTiles.map { $0.identifier }
        case .tilesReceived(let tilesResult):
            switch tilesResult {
            case .success(let response):
                newState.loadedTiles = response.tiles
                newState.offset = response.offset
                newState.showingTilesIds = response.tiles.map { $0.identifier }
                newState.shouldLoadTiles = false
                newState.failure = nil
            case .failure(let error):
                newState.shouldLoadTiles = false
                newState.failure = error
            }
        }
        return newState
    }
}

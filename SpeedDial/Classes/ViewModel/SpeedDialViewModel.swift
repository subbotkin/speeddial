//
//  SpeedDialViewModel.swift
//  AlohaBrowser
//
//  Created by ib on 30/10/2017.
//  Copyright © 2017 Aloha Mobile Ltd. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxFeedback

final class SpeedDialViewModel {

    private let disposeBag = DisposeBag()
    // DI
    private let imageService: ImageService
    private let feedService: FeedService

    func set(adViewController: UIViewController?) {
        self.feedService.set(adViewController: adViewController)
    }

    var isActive: Bool = false {
        didSet {
            bindingOut()
        }
    }

    // MARK: - Input

    let loadMoreTrigger: PublishSubject<()> = PublishSubject()

    // MARK: - Output

    let speedDialHeaderViewModel: SpeedDialHeaderViewModel
    let speedDialSection: BehaviorSubject<[SpeedDialSectionModel]>

    init() {
        speedDialSection = BehaviorSubject(value: [])

        let networkingServie = RxNetworkingImpl()
        imageService = DefaultImageService(dependencies: Dependencies.shared)
        let tilesService = TilesServiceImpl(dependencies: Dependencies.shared,
                                            networkingService: networkingServie)
        speedDialHeaderViewModel = SpeedDialHeaderViewModel(imageService: imageService, tilesService: tilesService)
        feedService = FeedServiceImpl(newsService: NewsServiceImpl(dependencies: Dependencies.shared,
                                                                   networkingService: networkingServie,
                                                                   database: NewsDatabase(version: 1)),
                                      adService: AdServiceImpl(),
                                      imageService: imageService)
        speedDialSection.onNext(mockSections)
    }

    // MARK: - Private

    private func bindingOut() {
        let loadMoreTrigger: (Driver<FeedState>) -> Signal<(FeedStateCommand)>  = { [weak self] state in
            guard let strongSelf = self else {
                return .empty()
            }
            return strongSelf.loadMoreTrigger
                .asSignal(onErrorJustReturn: ())
                .withLatestFrom(state)
                .flatMap { !$0.shouldLoadMoreNews ? .just(()) : .empty() }
                .map { FeedStateCommand.loadMoreNews }
        }

        let bindVM: FeedService.Feedback = bind(self) { viewModel, state in
            let subscriptions: [Disposable] = [
                state.map { ($0.feed.value, $0.loadingFeed) }
                    .map { $0.0 + $0.1 }
                    .drive(onNext: { (feedItems) in
                    let sectionItems = feedItems.map(SpeedDialSectionItem.feedItem)
                    guard var sections = try? viewModel.speedDialSection.value() else {
                        return
                    }
                    var newsSection = sections[2]
                    newsSection = SpeedDialSectionModel(original: newsSection, items: sectionItems)
                    sections[2] = newsSection
                    viewModel.speedDialSection.onNext(sections)
                })
            ]
            let events: [Signal<FeedStateCommand>] = [
                loadMoreTrigger(state)
            ]
            return Bindings(subscriptions: subscriptions, events: events)
        }

        feedService
            .fetchFeed(bind: bindVM)
            .drive()
            .disposed(by: disposeBag)
    }
}

extension SpeedDialViewModel: FeedProvider {

    func feedItem(at index: Int) -> FeedItem? {
        do {
            let feedSection = try speedDialSection.value()[2]
            let speedDialItem = feedSection.items[index]
            if case .feedItem(let feedItem) = speedDialItem {
                return feedItem
            } else {
                return nil
            }
        } catch {
            assertionFailure("Falure to get section")
            return nil
        }
    }
}

fileprivate extension SpeedDialViewModel {
    var mockSections: [SpeedDialSectionModel] {
        let bookmarksSection = SpeedDialSectionModel.speedDialBookmarksSection(items: mockBookmarks())
        let frequentlyUsedSection = SpeedDialSectionModel.frequentlyVisitedSection(title: "Frequently Used",
                                                                                   items: mockFrequentlyUsed())
        let news = SpeedDialSectionModel.newsSection(title: "News", items: [])

        return [bookmarksSection,
                frequentlyUsedSection,
                news]
    }

    func mockBookmarks() -> [SpeedDialSectionItem] {
        return [
            SpeedDialSectionItem.speedDialBookmarkItem(SpeedDialBookmarkViewModel())
        ]
    }

    func mockFrequentlyUsed() -> [SpeedDialSectionItem] {
        return [
            SpeedDialSectionItem.frequentlyVisitedItem(FrequentlyVisitedViewModel())
        ]
    }
}

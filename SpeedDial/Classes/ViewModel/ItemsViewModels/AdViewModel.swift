//
//  NewsAdViewModel.swift
//  SpeedDial
//
//  Created by ib on 02/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift

struct AdViewModel {
    // MARK: - Input

    private let disposeBag = DisposeBag()
    private let imageService: ImageService
    var adModel: AdModel {
        didSet {
            bind(to: adModel)
        }
    }

    // MARK: - Output

    var title: Observable<String?> { return _title.asObservable() }
    var subtitle: Observable<String?> { return _subtitle.asObservable() }
    var callToAction: Observable<String?> { return _callToAction.asObservable() }
    var adImage: Observable<UIImage?> { return _adImage.asObservable() }

    private let _title: BehaviorSubject<String?> = BehaviorSubject(value: nil)
    private let _subtitle: BehaviorSubject<String?> = BehaviorSubject(value: nil)
    private let _callToAction: BehaviorSubject<String?> = BehaviorSubject(value: nil)
    private let _adImage: ReplaySubject<UIImage?> = ReplaySubject.create(bufferSize: 1)
}

extension AdViewModel {
    init(adModel: AdModel, imageService: ImageService) {
        self.adModel = adModel
        self.imageService = imageService
        bind(to: adModel)
    }

    private func bind(to model: AdModel) {
        _title.onNext(model.title)
        _subtitle.onNext(model.subtitle)
        _callToAction.onNext(model.callToAction)
        if let iconUrl = model.iconUrl {
            imageService.imageFromURL(iconUrl)
                .map { downloadableImage -> UIImage? in
                    switch downloadableImage {
                    case .noImage: return nil
                    case .content(image: let image): return image
                    }
                }
                .bind(to: _adImage)
                .disposed(by: disposeBag)
        }
    }
}

extension AdViewModel: Equatable {
    static func == (lhs: AdViewModel, rhs: AdViewModel) -> Bool {
        return
            lhs.adModel.type == rhs.adModel.type &&
            lhs.adModel.title == rhs.adModel.title &&
            lhs.adModel.subtitle == rhs.adModel.subtitle &&
            lhs.adModel.iconUrl == rhs.adModel.iconUrl &&
            lhs.adModel.callToAction == rhs.adModel.callToAction
    }
}

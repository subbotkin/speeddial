//
//  SpeedDialItem.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift

protocol SpeedDialItem {
    var title: Observable<String?> { get set }
    var iconImage: Observable<UIImage?> { get set }
}

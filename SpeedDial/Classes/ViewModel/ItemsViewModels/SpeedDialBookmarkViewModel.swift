//
//  SpeedDialBookmarkViewModel.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift

struct SpeedDialBookmarkViewModel: SpeedDialItem {
    var title: Observable<String?>
    var iconImage: Observable<UIImage?>
}

extension SpeedDialBookmarkViewModel: Equatable {
    static func == (lhs: SpeedDialBookmarkViewModel, rhs: SpeedDialBookmarkViewModel) -> Bool {
        return true
    }
}

extension SpeedDialBookmarkViewModel {
    init() {
        self.title = Observable.just("Bookmark")
        self.iconImage = Observable.just(UIImage(named: "55"))
    }
}

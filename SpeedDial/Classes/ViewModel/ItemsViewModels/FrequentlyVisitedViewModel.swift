//
//  FrequentlyVisitedViewModel.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift

struct FrequentlyVisitedViewModel: SpeedDialItem {
    var title: Observable<String?>
    var iconImage: Observable<UIImage?>
}

extension FrequentlyVisitedViewModel: Equatable {
    static func == (lhs: FrequentlyVisitedViewModel, rhs: FrequentlyVisitedViewModel) -> Bool {
        return true
    }
}

extension FrequentlyVisitedViewModel {
    init() {
        self.title = Observable.just("Freq")
        self.iconImage = Observable.just(UIImage(named: "70"))
    }
}

//
//  FeedState.swift
//  SpeedDial
//
//  Created by ib on 14/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation

enum FeedStateCommand {
    case loadMoreNews
    case loading
    case newsReceived(NewsResponse)
    case adReceived([AdPair])
}

struct FeedState {
    // MARK: - DI
    private let imageService: ImageService

    // control
    private var shouldLoadCache: Bool
    private var newsOffset: String?
    private var newsCount: Int
    private var shouldLoadAd: Bool
    private var adRange: Range<Int>?

    var shouldLoadMoreNews: Bool
    var loadingFeed: [FeedItem]
    var feed: Version<[FeedItem]>
    var failure: NewsServiceError?

    init(imageService: ImageService, newsOffset: String?) {
        self.imageService = imageService
        self.newsOffset = newsOffset

        shouldLoadCache = true
        shouldLoadMoreNews = false
        shouldLoadAd = false
        feed = Version([])
        loadingFeed = []
        newsCount = 0
    }
}

extension FeedState {
    var nextPageOffset: String? {
        return self.shouldLoadMoreNews ? newsOffset : nil
    }

    var loadCache: String? {
        return self.shouldLoadCache ? newsOffset : nil
    }

    var loadAd: Range<Int>? {
        return shouldLoadAd ? adRange : nil
    }
}

extension FeedState {
    static func reduce(state: FeedState, command: FeedStateCommand) -> FeedState {
        var newState = state
        switch command {
        case .loadMoreNews:
            if state.failure == nil && state.newsOffset != nil {
                newState.shouldLoadMoreNews = true
            }
            newState.shouldLoadCache = false
        case .loading:
            newState.loadingFeed = Array(repeating: .loading(LoadingNewsViewModel()), count: 3)
            newState.shouldLoadMoreNews = true
            newState.shouldLoadCache = false
        case .adReceived(let ads):
            var feed = newState.feed.value
            for pair in ads {
                let adFeedItem = FeedItem.advertisment(AdViewModel(adModel: pair.advertisment,
                                                                   imageService: newState.imageService))
                if case .advertisment = feed[pair.index] {
                    feed.remove(at: pair.index)
                }
                feed.insert(adFeedItem, at: pair.index)
            }

            newState.feed = Version(feed)
            newState.shouldLoadAd = false
            newState.adRange = nil
        case .newsReceived(let newsResult):
            switch newsResult {
            case .success(let response):
                newState.loadingFeed = []

                newState.adRange = Range(newState.newsCount...(newState.newsCount + response.news.count))
                let responseNews = response.news
                    .map { NewsViewModel.init(news: $0, imageService: newState.imageService) }
                    .map(FeedItem.news)
                newState.feed = Version(newState.feed.value + responseNews)
                newState.newsOffset = response.offset
                newState.newsCount += response.news.count
                newState.shouldLoadMoreNews = false
                newState.shouldLoadCache = false
                newState.shouldLoadAd = true
                newState.failure = nil
            case .failure(let error):
                newState.loadingFeed = []
                newState.failure = error
                newState.shouldLoadCache = false
            }
        }
        return newState
    }
}

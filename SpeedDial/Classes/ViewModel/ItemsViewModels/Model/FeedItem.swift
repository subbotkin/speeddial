//
//  FeedItem.swift
//  SpeedDial
//
//  Created by ib on 09/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation

enum FeedItem {
    case news(NewsViewModel)
    case advertisment(AdViewModel)
    case loading(LoadingNewsViewModel)
}

extension FeedItem: Equatable {
    static func == (lhs: FeedItem, rhs: FeedItem) -> Bool {
        switch (lhs, rhs) {
        case (.news(let lhsViewModel), .news(let rhsViewModel)):
            return lhsViewModel.news == rhsViewModel.news
        case (.advertisment(let lhsViewModel), .advertisment(let rhsViewModel)):
            return lhsViewModel.adModel == rhsViewModel.adModel
        case (.loading(let lhs), .loading(let rhs)):
            return lhs.id == rhs.id
        default: return false
        }
    }
}

//
//  SpeedDialNewsViewModel.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxSwiftExt

struct NewsViewModel {

    // MARK: - Input

    let news: News
    private let disposeBag = DisposeBag()
    private let imageService: ImageService
    private let _thumbnailSubject: ReplaySubject<UIImage?> = ReplaySubject.create(bufferSize: 1)

    // MARK: - Output

    var newsThumbnail: Observable<UIImage?> {
        return _thumbnailSubject.asObservable()
    }
    let title: Observable<String>
    let subtitle: Observable<String?>
    let source: Observable<String?>
    let date: Observable<String?>
}

extension NewsViewModel {
    init(news: News, imageService: ImageService) {
        self.news = news
        self.imageService = imageService

        title = Observable.just(news.title)
        subtitle = Observable.just(news.subtitle)
        source = Observable.just(news.source)
        date = Observable.just(news.date).map { _ in "Some Date" }
        bindingOut()
    }

    private func bindingOut() {
        if let imageUrl = news.imageUrl {
            imageService.imageFromURL(imageUrl)
                .map { (downloadableImage) -> UIImage? in
                    switch downloadableImage {
                    case .noImage: return nil
                    case .content(image: let image): return image
                    }
                }
                .bind(to: _thumbnailSubject)
                .disposed(by: disposeBag)
        }
    }
}

extension NewsViewModel: Equatable {
    static func == (lhs: NewsViewModel, rhs: NewsViewModel) -> Bool {
        return lhs.news.title == rhs.news.title &&
            lhs.news.subtitle == rhs.news.subtitle &&
            lhs.news.imageUrl == rhs.news.imageUrl &&
            lhs.news.url == rhs.news.url &&
            lhs.news.date == rhs.news.date &&
            lhs.news.source == rhs.news.source
    }
}

//
//  ModalTileViewModel.swift
//  SpeedDial
//
//  Created by ib on 23/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct ModalTileViewModel {
    // MARK: - Properties
    private let disposeBag = DisposeBag()
    private let tile: Tile
    private let imageService: ImageService
    private let _tileImageSubject: ReplaySubject<UIImage?> = ReplaySubject.create(bufferSize: 1)

    // MARK: - Output

    let tileLoadUrl: URL?
    var tileImage: Driver<UIImage?> {
        return _tileImageSubject.asDriver(onErrorJustReturn: nil)
    }

    init(tile: Tile, imageService: ImageService) {
        self.tile = tile
        self.imageService = imageService
        self.tileLoadUrl = tile.adUrl

        if let imageUrl = tile.imageUrl {
            imageService.imageFromURL(imageUrl)
                .map { downloadableImage -> UIImage? in
                    switch downloadableImage {
                    case .noImage: return nil
                    case .content(image: let image): return image
                    }
                }
                .bind(to: _tileImageSubject)
                .disposed(by: disposeBag)
        }
    }
}

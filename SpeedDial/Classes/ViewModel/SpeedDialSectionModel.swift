//
//  SpeedDialSectionModel.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxDataSources

enum SpeedDialSectionModel {
    case speedDialBookmarksSection(items: [SpeedDialSectionItem])
    case frequentlyVisitedSection(title:String, items: [SpeedDialSectionItem])
    case newsSection(title: String, items: [SpeedDialSectionItem])

    var title: String? {
        switch self {
        case .speedDialBookmarksSection(items: _): return nil
        case .frequentlyVisitedSection(title: let title, items: _): return title
        case .newsSection(title: let title, items: _): return title
        }
    }
}

enum SpeedDialSectionItem {
    case speedDialBookmarkItem(SpeedDialBookmarkViewModel)
    case frequentlyVisitedItem(FrequentlyVisitedViewModel)
    case feedItem(FeedItem)
}

extension SpeedDialSectionModel: AnimatableSectionModelType {
    typealias Item = SpeedDialSectionItem

    var items: [SpeedDialSectionItem] {
        switch self {
        case .speedDialBookmarksSection(items: let items):
            return items
        case .frequentlyVisitedSection(title: _, items: let items):
            return items
        case .newsSection(title: _, items: let items):
            return items
        }
    }

    init(original: SpeedDialSectionModel, items: [Item]) {
        switch original {
        case .speedDialBookmarksSection:
            self = .speedDialBookmarksSection(items: items)
        case .frequentlyVisitedSection(let title, _):
            self = .frequentlyVisitedSection(title: title, items: items)
        case .newsSection(let title, _):
            self = .newsSection(title: title, items: items)
        }
    }

}

extension SpeedDialSectionModel: IdentifiableType {
    typealias Identity = String

    var identity: String {
        return title ?? "Unique title"
    }
}

extension SpeedDialSectionItem: Equatable {
    static func == (lhs: SpeedDialSectionItem, rhs: SpeedDialSectionItem) -> Bool {
        switch (lhs, rhs) {
        case (.speedDialBookmarkItem(let lhsViewModel), .speedDialBookmarkItem(let rhsViewModel)):
            return lhsViewModel == rhsViewModel
        case (.frequentlyVisitedItem(let lhsViewModel), .frequentlyVisitedItem(let rhsViewModel)):
            return lhsViewModel == rhsViewModel
        case (.feedItem(let lhsFeedItem), .feedItem(let rhsFeedItem)):
            return lhsFeedItem == rhsFeedItem
        default:
            return false
        }
    }
}

extension SpeedDialSectionItem: IdentifiableType {
    typealias Identity = String

    var identity: String {
        switch self {
        case .speedDialBookmarkItem(let speedDialBookmarkViewModel):
            return "SpeedDialViewModel"
        case .frequentlyVisitedItem(let freqVisitedViewModel):
            return "FrequentlyVisited"
        case .feedItem(let feedItem):
            switch feedItem {
            case .news(let newsViewModel):
                return newsViewModel.news.tempId
            case .advertisment(let advertismentViewModel):
                return advertismentViewModel.adModel.id
            case .loading:
                return "Loading"
            }
        }
    }
}

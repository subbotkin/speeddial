//
//  RawNewsResponse.swift
//  SpeedDial
//
//  Created by ib on 09/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation

struct RawNewsResponse {
    let news: [News]
    let offset: String
}

extension RawNewsResponse: Decodable {
    enum RootKeys: String, CodingKey {
        case news
        case offset
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootKeys.self)
        let news = try container.decode([News].self, forKey: .news)
        let offset = try container.decode(String.self, forKey: .offset)

        self.init(news: news, offset: offset)
    }
}

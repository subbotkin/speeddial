//
//  RawTilesResponse.swift
//  SpeedDial
//
//  Created by ib on 22/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation

struct RawTilesResponse {
    let tiles: [Tile]
    let offset: String?
}

extension RawTilesResponse: Decodable {
    enum RootKeys: String, CodingKey {
        case tiles
        case offset
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: RootKeys.self)
        let tiles = try container.decode([Tile].self, forKey: .tiles)
        let offset = try container.decode(String?.self, forKey: .offset)
        self.init(tiles: tiles, offset: offset)
    }
}

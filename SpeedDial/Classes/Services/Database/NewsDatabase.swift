//
//  NewsDatabase.swift
//  SpeedDial
//
//  Created by ib on 17/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import Realm
import RealmSwift

final class NewsDatabase {

    private let realm: Realm

    init(version: UInt64) {
        let fileUrl = URL(fileURLWithPath: RLMRealmPathForFile("\(String(describing: News.self)).realm"),
                          isDirectory: false)
        let configuration = Realm.Configuration(fileURL: fileUrl,
                                                readOnly: false,
                                                schemaVersion: version)
        do {
            self.realm = try Realm(configuration: configuration)
        } catch {
            fatalError("Cant instantiate realm with configuration \(configuration)")
        }
    }

    // MARK: - Public

    func objects() -> Results<News> {
        return realm.objects(News.self)
    }

    func store(_ model: News) {
        do {
            try realm.write {
                realm.add(model)
            }
        } catch {
            assertionFailure("Cant store model \(model), cause: \(error.localizedDescription)")
        }
    }

    func store(_ models: [News]) {
        do {
            try realm.write {
                realm.add(models)
            }
        } catch {
            assertionFailure("Cant store models \(models), cause: \(error.localizedDescription)")
        }
    }

    func delete(_ model: News) {
        do {
            try realm.write {
                realm.delete(model)
            }
        } catch {
            assertionFailure("Cant delete model \(model), cause: \(error.localizedDescription)")
        }
    }

    func delete() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            assertionFailure("Cant all models, cause: \(error.localizedDescription)")
        }
    }

    // MARK: - Private
}

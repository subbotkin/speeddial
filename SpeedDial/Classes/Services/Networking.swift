//
//  NetworkService.swift
//  AlohaBrowser
//
//  Created by Alexey Subbotkin on 13/07/2017.
//  Copyright © 2017 Aloha Mobile Ltd. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import RxAlamofire

enum NetworkingMethods {
    case get
}

protocol RxNetworking {
    func dataRequest(_ method: NetworkingMethods, _ url: URL, parameters: [String: Any]?) -> Observable<Data>
}

final class RxNetworkingImpl: RxNetworking {
    func dataRequest(_ method: NetworkingMethods, _ url: URL, parameters: [String: Any]?) -> Observable<Data> {
        return data(method.toAlamofire(), url, parameters: parameters)
    }
}

private extension NetworkingMethods {
    func toAlamofire() -> HTTPMethod {
        switch self {
        case .get: return .get
        }
    }
}

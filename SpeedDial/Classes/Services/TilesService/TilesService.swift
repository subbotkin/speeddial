//
//  TilesService.swift
//  SpeedDial
//
//  Created by ib on 22/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxAlamofire

enum TilesServiceError: Error {
    case cantParse(Error)
    case offline
    case networkError
}

typealias TilesResponse = Result<(tiles: [Tile], offset: String?), TilesServiceError>
protocol TilesService {
    func fetchTiles(with offset: String?, showingTiles: [Int]) -> Observable<TilesResponse>
}

final class TilesServiceImpl: TilesService {

    // MARK: - Properties
    private let reachabilityService: ReachabilityService
    private let backgroundWorkSchedular: ImmediateSchedulerType
    private let networkingService: RxNetworking

    init(dependencies: ServiceDependencies, networkingService: RxNetworking) {
        reachabilityService = dependencies.reachabilityService
        backgroundWorkSchedular = dependencies.backgroundWorkScheduler
        self.networkingService = networkingService
    }

    func fetchTiles(with offset: String?, showingTiles: [Int]) -> Observable<TilesResponse> {
        guard let fetchUrl = URL(string: "https://api.alohabrowser.com/v1/speed_dial_tiles") else {
            assertionFailure("Wrong fetch url")
            return .empty()
        }
        // TODO: Provide country and ids
        var parameters: [String: Any] = ["os": "ios", "countryCode": "cy", "ids": showingTiles]
        if let offset = offset {
            parameters["offset"] = offset
        }

        return networkingService.dataRequest(.get, fetchUrl, parameters: parameters)
            .retry(3)
            .observeOn(backgroundWorkSchedular)
            .map { data -> TilesResponse in
                do {
                    let decoder = JSONDecoder()
                    let response = try decoder.decode(RawTilesResponse.self, from: data)
                    // TODO: Save on database
                    return .success((tiles:response.tiles, offset: response.offset))
                } catch {
                    return .failure(.cantParse(error))
                }
            }
            .retryOnBecomesReachable(.failure(.offline), reachabilityService: reachabilityService)
    }
}

//
//  ServiceDependency.swift
//  SpeedDial
//
//  Created by ib on 14/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift

protocol ServiceDependencies: class {
    var reachabilityService: ReachabilityService { get }
    var backgroundWorkScheduler: ImmediateSchedulerType { get }
}

final class Dependencies: ServiceDependencies {

    static let shared = Dependencies()

    let reachabilityService: ReachabilityService
    let backgroundWorkScheduler: ImmediateSchedulerType

    private init() {
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 2
        #if !RX_NO_MODULE
            operationQueue.qualityOfService = QualityOfService.userInitiated
        #endif
        backgroundWorkScheduler = OperationQueueScheduler(operationQueue: operationQueue)
        do {
            reachabilityService = try DefaultReachabilityService()
        } catch {
            assertionFailure("Cant instantiate reachabilityService")
            reachabilityService = AlwaysReachableService()
        }
    }
}

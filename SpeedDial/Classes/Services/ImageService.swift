//
//  ImageService.swift
//  SpeedDial
//
//  Created by ib on 09/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit
import RxSwift
import RxAlamofire
import RxCocoa

enum DownloadableImage {
    case content(image: UIImage)
    case noImage
}

enum ImageServiceError: Error {
    case apiError(String)
}

protocol ImageService {
    func imageFromURL(_ url: URL) -> Observable<DownloadableImage>
}

final class DefaultImageService: ImageService {
    
    private let backgroundWorkScheduler: ImmediateSchedulerType
    private let reachabilityService: ReachabilityService

    // TODO: Change Cache
    // 1st level cache
    private let _imageCache = NSCache<AnyObject, AnyObject>()

    // 2nd level cache
    private let _imageDataCache = NSCache<AnyObject, AnyObject>()

    init(dependencies: ServiceDependencies) {
        reachabilityService = dependencies.reachabilityService
        backgroundWorkScheduler = dependencies.backgroundWorkScheduler
        // cost is approx memory usage
        _imageDataCache.totalCostLimit = 10 * 1024 * 1024

        _imageCache.countLimit = 20
    }

    private func decodeImage(_ imageData: Data) -> Observable<UIImage> {
        return Observable.just(imageData)
            .observeOn(backgroundWorkScheduler)
            .map { data in
                guard let image = UIImage(data: data) else {
                    // some error
                    throw ImageServiceError.apiError("Decoding image error")
                }
                return image.forceLazyImageDecompression()
        }
    }

    private func _imageFromURL(_ url: URL) -> Observable<UIImage> {
        return Observable.deferred {
            let maybeImage = self._imageCache.object(forKey: url as AnyObject) as? UIImage

            let decodedImage: Observable<UIImage>

            // best case scenario, it's already decoded an in memory
            if let image = maybeImage {
                decodedImage = Observable.just(image)
            } else {
                let cachedData = self._imageDataCache.object(forKey: url as AnyObject) as? Data

                // does image data cache contain anything
                if let cachedData = cachedData {
                    decodedImage = self.decodeImage(cachedData)
                } else {
                    // fetch from network
                    decodedImage = requestData(URLRequest(url: url))
                        .map { $0.1 }
                        .do(onNext: { data in
                            self._imageDataCache.setObject(data as AnyObject, forKey: url as AnyObject)
                        })
                        .flatMap(self.decodeImage)
                }
            }

            return decodedImage.do(onNext: { image in
                self._imageCache.setObject(image, forKey: url as AnyObject)
            })
        }
    }

    /**
     Service that tries to download image from URL.

     In case there were some problems with network connectivity and image wasn't downloaded,
     automatic retry will be fired when networks becomes available.

     After image is successfully downloaded, sequence is completed.
     */
    func imageFromURL(_ url: URL) -> Observable<DownloadableImage> {
        return _imageFromURL(url)
            .map { DownloadableImage.content(image: $0) }
            .retryOnBecomesReachable(DownloadableImage.noImage, reachabilityService: reachabilityService)
            .startWith(.noImage)
    }
}

extension UIImage {
    func forceLazyImageDecompression() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        self.draw(at: CGPoint.zero)
        UIGraphicsEndImageContext()
        return self
    }
}

//
//  GADAdLoader+Rx.swift
//  SpeedDial
//
//  Created by ib on 10/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import GoogleMobileAds
import RxSwift
import RxCocoa

final class RxGADAdLoaderDelegateProxy: DelegateProxy<GADAdLoader, GADAdLoaderDelegate>,
    DelegateProxyType,
    GADAdLoaderDelegate,
    GADNativeAppInstallAdLoaderDelegate,
    GADNativeContentAdLoaderDelegate {

    init(parentObject: GADAdLoader) {
        super.init(parentObject: parentObject, delegateProxy: RxGADAdLoaderDelegateProxy.self)
    }

    static func registerKnownImplementations() {
        register { RxGADAdLoaderDelegateProxy(parentObject: $0) }
    }

    static func currentDelegate(for object: GADAdLoader) -> GADAdLoaderDelegate? {
        return object.delegate
    }

    static func setCurrentDelegate(_ delegate: GADAdLoaderDelegate?, to object: GADAdLoader) {
        object.delegate = delegate
    }

    // MARK: - Subjects

    fileprivate let receiveAdErrorSubject: PublishSubject<GADRequestError> = PublishSubject()
    fileprivate let receiveNativeAppInstalAd: PublishSubject<GADNativeAppInstallAd> = PublishSubject()
    fileprivate let receiveNativeContentAd: PublishSubject<GADNativeContentAd> = PublishSubject()

    // MARK: - GADAdLoaderDelegate

    func adLoader(_ adLoader: GADAdLoader, didFailToReceiveAdWithError error: GADRequestError) {
        receiveAdErrorSubject.onNext(error)
        (_forwardToDelegate as? GADAdLoaderDelegate)?.adLoader(adLoader, didFailToReceiveAdWithError: error)
    }

    // MARK: - GADNativeAppInstallAdLoaderDelegate

    func adLoader(_ adLoader: GADAdLoader, didReceive nativeAppInstallAd: GADNativeAppInstallAd) {
        receiveNativeAppInstalAd.onNext(nativeAppInstallAd)
        (_forwardToDelegate as? GADNativeAppInstallAdLoaderDelegate)?.adLoader(adLoader, didReceive: nativeAppInstallAd)
    }

    // MARK: - GADNativeContentAdLoaderDelegate

    func adLoader(_ adLoader: GADAdLoader, didReceive nativeContentAd: GADNativeContentAd) {
        receiveNativeContentAd.onNext(nativeContentAd)
        (_forwardToDelegate as? GADNativeContentAdLoaderDelegate)?.adLoader(adLoader, didReceive: nativeContentAd)
    }

    deinit {
        receiveAdErrorSubject.onCompleted()
        receiveNativeAppInstalAd.onCompleted()
        receiveNativeContentAd.onCompleted()
    }
}

extension Reactive where Base: GADAdLoader {

    var delegate: DelegateProxy<GADAdLoader, GADAdLoaderDelegate> {
        return RxGADAdLoaderDelegateProxy.proxy(for: base)
    }

    var didFailReceiveAd: Observable<GADRequestError> {
        return (delegate as? RxGADAdLoaderDelegateProxy)?.receiveAdErrorSubject.asObservable() ?? .empty()
    }

    var didReceiveNativeAppInstallAd: Observable<GADNativeAppInstallAd> {
        return (delegate as? RxGADAdLoaderDelegateProxy)?.receiveNativeAppInstalAd.asObservable() ?? .empty()
    }

    var didReceiverNativeContentAd: Observable<GADNativeContentAd> {
        return (delegate as? RxGADAdLoaderDelegateProxy)?.receiveNativeContentAd.asObservable() ?? .empty()
    }
}

//
//  FBNativeAd+Rx.swift
//  SpeedDial
//
//  Created by ib on 13/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import FBAudienceNetwork
import RxSwift
import RxCocoa

final class RxFBNativeAdDelegateProxy: DelegateProxy<FBNativeAd, FBNativeAdDelegate>,
    DelegateProxyType,
    FBNativeAdDelegate {

    init(parentObject: FBNativeAd) {
        super.init(parentObject: parentObject, delegateProxy: RxFBNativeAdDelegateProxy.self)
    }

    static func registerKnownImplementations() {
        register { RxFBNativeAdDelegateProxy(parentObject: $0) }
    }

    static func currentDelegate(for object: FBNativeAd) -> FBNativeAdDelegate? {
        return object.delegate
    }

    static func setCurrentDelegate(_ delegate: FBNativeAdDelegate?, to object: FBNativeAd) {
        object.delegate = delegate
    }
}

extension Reactive where Base: FBNativeAd {
    var delegate: DelegateProxy<FBNativeAd, FBNativeAdDelegate> {
        return RxFBNativeAdDelegateProxy.proxy(for: base)
    }

    var nativeAdDidLoad: Observable<FBNativeAd> {
        return delegate
            .methodInvoked(#selector(FBNativeAdDelegate.nativeAdDidLoad(_:)))
            .filterMap { parameters in
                guard let nativeAd = parameters[0] as? FBNativeAd else {
                    assertionFailure("Wrong parameter type \(type(of: parameters[0]))")
                    return .ignore
                }
                return .map(nativeAd)
        }
    }

    var nativeAdLoadDidFail: Observable<(FBNativeAd, NSError)> {
        return delegate
            .methodInvoked(#selector(FBNativeAdDelegate.nativeAd(_:didFailWithError:)))
            .filterMap { parameters in
                guard
                    let nativeAd = parameters[0] as? FBNativeAd,
                    let error = parameters[1] as? NSError else {
                        assertionFailure("Wrong parameters \(parameters)")
                        return .ignore
                }
                return .map ((nativeAd, error))
        }
    }
}

//
//  FeedService.swift
//  SpeedDial
//
//  Created by ib on 09/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxFeedback

protocol FeedService {
    typealias Feedback = (Driver<FeedState>) -> Signal<FeedStateCommand>
    func set(adViewController: UIViewController?)
    func fetchFeed(bind: @escaping Feedback) -> Driver<FeedState>
}

final class FeedServiceImpl: FeedService {
    private let newsService: NewsService
    private let adService: AdService
    private let imageService: ImageService

    init(newsService: NewsService, adService: AdService, imageService: ImageService) {
        self.newsService = newsService
        self.adService = adService
        self.imageService = imageService
    }

    func set(adViewController: UIViewController?) {
        self.adService.adViewController = adViewController
    }

    func fetchFeed(bind: @escaping Feedback) -> Driver<FeedState> {
        let fetchFeedback: (Driver<FeedState>) -> Signal<FeedStateCommand> =
            react(query: { $0.nextPageOffset },
                  effects: { [weak self] (offset) in
                    guard let strongSelf = self else {
                        return .empty()
                    }
                    return strongSelf.newsService.fetchNews(with: offset)
                        .asSignal(onErrorJustReturn: .failure(NewsServiceError.networkError))
                        .map(FeedStateCommand.newsReceived)
            })

        let cacheFeedbackLoop: (Driver<FeedState>) -> Signal<FeedStateCommand> =
            react(query: { $0.loadCache },
                  effects: { [weak self] offset in
                    guard let strongSelf = self else {
                        return .empty()
                    }
                    return strongSelf.newsService.cachedNews()
                        .asSignal(onErrorJustReturn: [])
                        .flatMap { cachedNews in
                            if cachedNews.isEmpty {
                                return .just(.loading)
                            } else {
                                return .just(.newsReceived(.success((cachedNews, offset))))
                            }
                    }
            })

        let adFeedbackLoop: (Driver<FeedState>) -> Signal<FeedStateCommand> =
            react(query: { $0.loadAd }, effects: { [weak self] adRange in
                guard let strongSelf = self else {
                    return .empty()
                }
                return strongSelf.adService.fetchAd(with: adRange)
                    .asSignal(onErrorJustReturn: [])
                    .map(FeedStateCommand.adReceived)
            })
        
        return Driver.system(initialState: FeedState(imageService: imageService, newsOffset: ""),
                             reduce: FeedState.reduce,
                             feedback: fetchFeedback, bind, cacheFeedbackLoop, adFeedbackLoop)
    }
}

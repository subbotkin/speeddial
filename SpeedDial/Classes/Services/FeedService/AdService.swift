//
//  AdLoaderService.swift
//  SpeedDial
//
//  Created by ib on 10/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import FBAudienceNetwork
import GoogleMobileAds
import RxSwift

typealias AdPair = (index: Int, advertisment: AdModel)

protocol AdService: class {
    var adViewController: UIViewController? { get set }
    func fetchAd(with range: Range<Int>) -> Observable<[AdPair]>
}

final class AdServiceImpl {

    // MARK: - Constants

    private enum AdConstans {
        static let googleAdUnitId = "ca-app-pub-7364574998956639/8020620827"
        static let facebookPlacementId = "425098947871163_488227151558342"
    }

    private var adMobIndexes: [Int] = [1, 10]
    private var fbAdIndex: [Int] = [5, 15]

    // MARK: - Properties

    private let disposeBag = DisposeBag()
    var adViewController: UIViewController?

    init() {
        FBAdSettings.addTestDevice("cc0be3afb23f98d2f10eb26e590346f43aa44a12")
    }

    private func gAdLoader() -> GADAdLoader {
        let adChoicesViewPosition = GADNativeAdViewAdOptions()
        adChoicesViewPosition.preferredAdChoicesPosition = .topRightCorner
        let imageLoadingOptions = GADNativeAdImageAdLoaderOptions()
        imageLoadingOptions.disableImageLoading = true

        let options: [GADAdLoaderOptions] = [adChoicesViewPosition, imageLoadingOptions]
        return GADAdLoader(adUnitID: AdConstans.googleAdUnitId,
                           rootViewController: adViewController,
                           adTypes: [.nativeAppInstall, .nativeContent],
                           options: options)
    }
    private func fbAdLoader() -> FBNativeAd {
        return FBNativeAd(placementID: AdConstans.facebookPlacementId)
    }

    private func loadGoogleAd(at index: Int) -> Observable<[AdPair]> {
        let loader = gAdLoader()
        let errorHandler: Observable<[AdPair]> = loader.rx.didFailReceiveAd.map { _ in return [] }
        let responseHandler = Observable
            .merge([loader.rx.didReceiveNativeAppInstallAd.map(AdModel.init(googleAppInstallAd:)),
                    loader.rx.didReceiverNativeContentAd.map(AdModel.init(googleContentAd:))])
            .take(1)
            .map { [AdPair(index: index, advertisment: $0)] }
        let handler = Observable.merge(responseHandler, errorHandler)

        return Observable.create({ observer in
            let request = DFPRequest()
            request.testDevices = [kGADSimulatorID, "49526dbd6ce3443669ba0fcc30a93c08"]
            loader.load(request)
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create { }
        })
            .flatMap({ handler })
    }

    private func loadFacebookAd(at index: Int) -> Observable<[AdPair]> {
        let loader = fbAdLoader()
        let errorHandler: Observable<[AdPair]> = loader.rx.nativeAdLoadDidFail.map { _ in [] }
        let responseHandler = loader.rx.nativeAdDidLoad
            .map(AdModel.init(fbAd:))
            .take(1)
            .map({ [AdPair(index: index, advertisment: $0)] })
        let handler = Observable.merge(errorHandler, responseHandler)
        return Observable.create({ observer in
            loader.load()
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create { }
        })
            .flatMap({ handler })
    }
}

extension AdServiceImpl: AdService {

    func fetchAd(with range: Range<Int>) -> Observable<[AdPair]> {
        var loadAdObs: [Observable<[AdPair]>] = []
        for googleIndex in adMobIndexes where range ~= googleIndex {
            loadAdObs.append(loadGoogleAd(at: googleIndex))
        }

        for fbIndex in fbAdIndex where range ~= fbIndex {
            loadAdObs.append(loadFacebookAd(at: fbIndex))
        }

        if loadAdObs.isEmpty {
            return .just([])
        } else {
            return Observable.combineLatest(loadAdObs).map { $0.flatMap { $0 } }
        }
    }
}

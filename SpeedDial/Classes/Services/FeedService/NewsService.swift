//
//  NewsService.swift
//  SpeedDial
//
//  Created by ib on 09/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import RxAlamofire

enum NewsServiceError: Error {
    case cantParse(Error)
    case offline
    case networkError
}

typealias NewsResponse = Result<(news: [News], offset: String?), NewsServiceError>

protocol NewsService: class {
    func fetchNews(with offset: String?) -> Observable<NewsResponse>
    func cachedNews() -> Observable<[News]>
}

final class NewsServiceImpl: NewsService {

    private let reachabilityService: ReachabilityService
    private let backgroundWorkScheduler: ImmediateSchedulerType
    private let database: NewsDatabase
    private let networkingService: RxNetworking

    init(dependencies: ServiceDependencies, networkingService: RxNetworking, database: NewsDatabase) {
        reachabilityService = dependencies.reachabilityService
        backgroundWorkScheduler = dependencies.backgroundWorkScheduler
        self.database = database
        self.networkingService = networkingService
    }

    func cachedNews() -> Observable<[News]> {
        let cachedNews = Array(self.database.objects())
        return .just(cachedNews)
    }

    func fetchNews(with offset: String?) -> Observable<NewsResponse> {
        guard let url = URL(string: "https://api.alohabrowser.com/v1/news") else {
            return .empty()
        }
        // TODO: Provide language
        var parameters: [String: Any] = ["os": "ios", "language": "en"]
        if let offset = offset {
            parameters["offset"] = offset
        }

        return networkingService.dataRequest(.get, url, parameters: parameters)
            .retry(3)
            .observeOn(backgroundWorkScheduler)
            .map { [weak self] data -> NewsResponse in
                do {
                    let decoder = JSONDecoder()
                    decoder.dateDecodingStrategy = .millisecondsSince1970
                    let response = try decoder.decode(RawNewsResponse.self, from: data)
                    DispatchQueue.main.async {
                        self?.database.store(response.news)
                    }
                    return .success((news: response.news, offset: response.offset))
                } catch {
                    return .failure(.cantParse(error))
                }
            }
            .retryOnBecomesReachable(.failure(.offline), reachabilityService: reachabilityService)
    }
}

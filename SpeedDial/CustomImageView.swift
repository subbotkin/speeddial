//
//  CustomImageView.swift
//  SpeedDial
//
//  Created by ib on 01/11/2017.
//  Copyright © 2017 ib. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {

    override var frame: CGRect {
        didSet {
            print(frame)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        print(frame)
    }

}

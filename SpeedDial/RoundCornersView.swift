//
//  RoundCornersView.swift
//  AlohaBrowser
//
//  Created by ib on 03/10/2017.
//  Copyright © 2017 Aloha Mobile Ltd. All rights reserved.
//

import UIKit

@IBDesignable
final class RoundCornersView: UIView {

    // MARK: - Public

    var roundedCorners: UIRectCorner = UIRectCorner.allCorners
    @IBInspectable var cornerRadius: CGSize = .zero

    // MARK: - Private

    private lazy var roundCornersLayer: CAShapeLayer = { [weak self] in
        let layer = CAShapeLayer()
        self?.layer.mask = layer
        return layer
    }()

    // MARK: - UIView

    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        roundCornersLayer.frame = layer.bounds
        let mask = UIBezierPath(roundedRect: roundCornersLayer.bounds,
                                byRoundingCorners: roundedCorners,
                                cornerRadii: cornerRadius)
        roundCornersLayer.path = mask.cgPath
    }
}
